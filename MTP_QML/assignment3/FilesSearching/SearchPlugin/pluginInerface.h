#ifndef PLUGININERFACE_H
#define PLUGININERFACE_H

#include <QList>
#include <QFileInfo>
#include <QDate>

#include <QtConcurrent/QtConcurrent>
#include <QtConcurrent/QtConcurrentFilter>
#include <QFuture>
#include <QFutureWatcher>

class PluginInterface{
public:
  QFuture<QFileInfo> m_future;
  QFutureWatcher<QFileInfo> watcher;
  virtual ~PluginInterface(){}
  virtual void proccessing(QList<QFileInfo>, QString, QDate, qint64) =0;
  virtual void cancel()=0;
private:
  virtual bool grep(const QFileInfo&,QString, QDate, qint64) =0;
};

Q_DECLARE_INTERFACE(PluginInterface, "ru.mephi.mtp")
#endif // PLUGININERFACE_H
