#include "searchplugin.h"

#include <QRegularExpression>
#include <QRegularExpressionMatch>

SearchPlugin::SearchPlugin(QObject*)
{
}
//____________________________________
SearchPlugin::~SearchPlugin()
{

}
//____________________________________
bool SearchPlugin::grep(const QFileInfo &file, QString pattern, QDate date, qint64 size)
{
    if(pattern.isEmpty()) return false;
    QString wildcard = QRegularExpression::wildcardToRegularExpression(pattern);
    QRegularExpression re(wildcard);
    if(re.match(file.fileName()).hasMatch()){
        bool dateBool = date.isNull() ? true : (file.birthTime().date()>=date);
        bool sizeBool = (size<0) ? true :  (file.size()<=size);
        return (dateBool && sizeBool);
    }
    else
        return false;

}
//____________________________________
void SearchPlugin::proccessing(QList<QFileInfo> list,
                               QString pattern, QDate date, qint64 size){
    m_future = QtConcurrent::filtered(list,  [=](const QFileInfo &f) {
        return grep(f,pattern, date, size);
    });
    watcher.setFuture(m_future);
}
//____________________________________
void SearchPlugin::cancel(){
    watcher.cancel();
    watcher.waitForFinished();
}
