#ifndef SEARCHPLUGIN_H
#define SEARCHPLUGIN_H
#include <QObject>

#include "SearchPlugin_global.h"
#include "pluginInerface.h"

class SEARCHPLUGIN_EXPORT SearchPlugin: public QObject, public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ru.mephi.mtp")
    Q_INTERFACES(PluginInterface)
public:
    explicit SearchPlugin(QObject* parent = nullptr);
    ~SearchPlugin() override;
    void proccessing(QList<QFileInfo>, QString, QDate, qint64) override;
    void cancel() override;

private:
    bool grep(const QFileInfo&,QString, QDate, qint64) override;
};
#endif // SEARCHPLUGIN_H
