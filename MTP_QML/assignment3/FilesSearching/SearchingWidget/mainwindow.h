#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QFileInfoList>

#include "../SearchPlugin/pluginInerface.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QSettings* settings,bool* pluginLoaded, QWidget *parent = nullptr);
    ~MainWindow();
    QDate dateToCompare();
    qint64 sizeToCompare();
    QFileInfoList getFilesInDir();
    QString getFilePattern();
private:
    Ui::MainWindow *ui;
    QSettings* m_settings;
    PluginInterface* interface;
    QFileInfoList m_files;
    void writeSettings();
    void readSettings();
    void closeEvent(QCloseEvent*);
    void createResultWidget();
    void clearTable();
    PluginInterface* loadPlugin();
private slots:
    void on_browseButton_clicked();
    // In order to save in QSettings last used values
    void fileNameCurrChanged(int);
    void dirCurrChanged(int);
//    void dateSpinBoxChanged(const QString &);
//    void sizeSpinBoxChanged(const QString &);
public slots:
    void startProcess();
    void cancelProcess();
    void fillTable();

};
#endif // MAINWINDOW_H
