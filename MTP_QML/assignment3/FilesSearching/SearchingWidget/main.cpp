#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("MySoft");
    QCoreApplication::setApplicationName("FilesSearcher");
    QSettings settings;
    bool pluginLoaded = false;
    MainWindow w(&settings, &pluginLoaded);
    if(pluginLoaded) w.show();
    else return 1;
    return a.exec();
}
