#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <limits>

#include <QLibraryInfo>
#include <QDir>
#include <QFileDialog>
#include <QDate>
#include <QCloseEvent>
#include <QDebug>
#include <QMessageBox>
#include <QFuture>
#include <QPluginLoader>

PluginInterface* loadProcess(QFileInfoList list, QStringList plugins){
    foreach(QFileInfo file, list){
      plugins.append(file.filePath());
    }
    foreach(QString file, plugins){
      QPluginLoader loader(file);
      if(!loader.load()){continue;}
      PluginInterface* plugin = qobject_cast<PluginInterface*>(loader.instance());
      if(plugin){
         qDebug()<<"Plugin " << loader.fileName() << "supports PluginInterface" ;
         return plugin;
      }
      else{continue;}
    }
    return nullptr;
}
//____________________________________
PluginInterface* MainWindow::loadPlugin(){
    QStringList filters;
    filters << "*.dll" << "*.so" << "*.dylib";

    // Check current directory
    QDir dir = QDir::current();
    QFileInfoList list = dir.entryInfoList(filters);
    // Check debug and release directories in SearchPlugin
    dir.cd("../SearchPlugin");
    dir.cd("debug");
    list += dir.entryInfoList(filters);
    dir.cd("../release");
    list += dir.entryInfoList(filters);

    QStringList plugins;
    PluginInterface* interface = loadProcess(list,plugins);
    if (interface!=nullptr) return interface;

   // If nothing is found, check standard path for plugins
     dir = QDir(QLibraryInfo::location(QLibraryInfo::PluginsPath));
     list.clear();
     list = dir.entryInfoList(filters);
     QFileInfoList genList = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);
     foreach(QFileInfo file, genList){
         if(file.isDir()){
           QDir subdir = QDir(file.absoluteFilePath());
           qDebug()<< subdir.absolutePath();
           list += subdir.entryInfoList(filters);
         }
     }
     plugins.clear();
     interface = loadProcess(list,plugins);
     if (interface!=nullptr) return interface;

   // If there is nothing, ask user to choose
     QString fileName = QFileDialog::getOpenFileName(this, QString("Open File"),
                                                     QString("../"),
                                                     QString("Plugins (*.dll *.so *.dylib)"));
     list.clear(); list << fileName;
     plugins.clear();
     interface = loadProcess(list,plugins);
     if (interface!=nullptr) return interface;

     QMessageBox msgBox;
     msgBox.setText("Sorry,the plugin does not support PluginInterface.");
     msgBox.exec();
     return nullptr;

}
//____________________________________
MainWindow::MainWindow(QSettings* settings, bool* pluginLoaded, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    ,m_settings(settings)
{
    ui->setupUi(this);
    readSettings();

    ui->dateSpinBox->setMinimum(0);
    ui->sizeSpinBox->setMinimum(0);
    ui->dateSpinBox->setMaximum(std::numeric_limits<int>::max());
    ui->sizeSpinBox->setMaximum(std::numeric_limits<int>::max());
    ui->resultWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->resultWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

    connect( ui->fileNameBox,SIGNAL(currentIndexChanged(int)),
             this, SLOT(fileNameCurrChanged(int)) );
    connect( ui->dirBox,SIGNAL(currentIndexChanged(int)),
             this, SLOT(dirCurrChanged(int)) );
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(startProcess()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancelProcess()));

    interface = loadPlugin();
    if(interface!=nullptr){
        connect(&(interface->watcher), SIGNAL(finished()),this, SLOT(fillTable()));
        connect(&(interface->watcher), SIGNAL(canceled()),this, SLOT(fillTable()));
        *pluginLoaded = true;
    }
    else
        *pluginLoaded = false;
}
//____________________________________
MainWindow::~MainWindow()
{
    delete ui;
}
//____________________________________
static void updateComboBox(QComboBox *comboBox)
{
    if (comboBox->findText(comboBox->currentText()) == -1)
        comboBox->addItem(comboBox->currentText());
}
//____________________________________
void MainWindow::writeSettings()
{
    m_settings->beginGroup("MainWindow");
    m_settings->setValue("size", size());
    m_settings->setValue("pos", pos());
    m_settings->setValue("dirName", ui->dirBox->itemText(ui->dirBox->count()-1));
    m_settings->setValue("filePattern", ui->fileNameBox->itemText(ui->fileNameBox->count()-1));
    m_settings->setValue("fileBirth", ui->dateSpinBox->value());
    m_settings->setValue("fileSize", ui->sizeSpinBox->value());
    m_settings->endGroup();
}
//____________________________________
void MainWindow::readSettings()
{
    m_settings->beginGroup("MainWindow");
    resize(m_settings->value("size", QSize(500, 650)).toSize());
    move(m_settings->value("pos", QPoint(200, 200)).toPoint());
    ui->dirBox->addItem(m_settings->value("dirName","").toString());
    ui->fileNameBox->addItem(m_settings->value("filePattern","*").toString());
    ui->dateSpinBox->setValue(m_settings->value("fileBirth",0).toInt());
    ui->sizeSpinBox->setValue(m_settings->value("fileSize",0).toInt());
    m_settings->endGroup();
}
//____________________________________
void MainWindow::on_browseButton_clicked()
{
    //qDebug()<< QDir::tempPath();
    QString directory = QDir::toNativeSeparators(
                            QFileDialog::getExistingDirectory(
                                this, tr("Find Files"), QDir::currentPath() ) );
    if (!directory.isEmpty()) {
        if (ui->dirBox->findText(directory) == -1)
            ui->dirBox->addItem(directory);
        ui->dirBox->setCurrentIndex(ui->dirBox->findText(directory));
    }
}
//____________________________________
void MainWindow::fileNameCurrChanged(int indx)
{
    if (indx == -1) return;
    if(indx!=ui->fileNameBox->count()-1){
        ui->fileNameBox->addItem(ui->fileNameBox->currentText());
        ui->fileNameBox->setCurrentIndex(ui->fileNameBox->count()-1);
        ui->fileNameBox->removeItem(indx);
    }
}
//____________________________________
void MainWindow::dirCurrChanged(int indx)
{
    if (indx == -1) return;
    if(indx!=ui->dirBox->count()-1){
        ui->dirBox->addItem(ui->dirBox->currentText());
        ui->dirBox->setCurrentIndex(ui->dirBox->count()-1);
        ui->dirBox->removeItem(indx);
    }
}
//____________________________________
void MainWindow::startProcess()
{
    clearTable();
    QFileInfoList filesFromDir = getFilesInDir();
    QString pattern = getFilePattern();
    QDate maxDate = dateToCompare();
    qint64 maxSize = sizeToCompare();
    updateComboBox(ui->dirBox);
    updateComboBox(ui->fileNameBox);
    interface->proccessing(filesFromDir, pattern, maxDate, maxSize);
}
//____________________________________
void MainWindow::cancelProcess()
{
    clearTable();
    if(interface->watcher.isFinished()){return;}
    QMessageBox msgBox;
    msgBox.setText("Process will be stoped. Wait for this, please");
    msgBox.exec();
    interface->cancel();
    msgBox.close();
}
//____________________________________
QDate MainWindow::dateToCompare(){
    if (!ui->dateCheckBox->isChecked()) return QDate(); // null date (Note: it is valid)
    QDate currDate = QDate::currentDate();
    int daysAgo = ui->dateSpinBox->value();
    return currDate.addDays(-1*daysAgo);// null date is in bad case
}
//____________________________________
qint64 MainWindow::sizeToCompare()
{
   if(!ui->sizeCheckBox->isChecked()) return static_cast<qint64>(-1);
    const int kilo = 1024;
    return static_cast<qint64>( (ui->sizeSpinBox->value())*kilo );
}
//____________________________________
QFileInfoList MainWindow::getFilesInDir()
{
    QDir dir = QDir::toNativeSeparators(ui->dirBox->currentText());
    if(!dir.isReadable()){
        return QFileInfoList();//empty list
    }
    QFileInfoList list = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);
    //QFileInfoList genList = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);
    for(int i=0;i<list.length();i++){
        if(list.at(i).isDir()){
          QDir subdir = QDir(list.at(i).absoluteFilePath());
          //qDebug()<< subdir.absolutePath();
          list += subdir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);
          //list.removeAt(i);
        }
        //else{i++;}
    }
    return list;
}
//____________________________________
QString MainWindow::getFilePattern()
{
    return ui->fileNameBox->currentText();
}
//____________________________________
void MainWindow::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}
//____________________________________
void MainWindow::fillTable(){
 QFuture<QFileInfo>::const_iterator it = interface->watcher.future().begin();
 if(interface->watcher.future().resultCount()==0){
     QMessageBox msgBox;
     msgBox.setText("Sorry, nothing was found.");
     msgBox.exec();
     return;
 }
 // get summary for searching proccess
 int files = 0; int dirs = 0;
 for(;it!=interface->watcher.future().end();it++){
     if( (*it).isDir() ) dirs++;
     else if( (*it).isFile() ) files++;
 }
 int row = ui->resultWidget->rowCount();
 ui->resultWidget->insertRow(row);
 QTableWidgetItem* summary =
         new QTableWidgetItem(QString("%1 files, %2 directories were found").arg(files).arg(dirs));
 ui->resultWidget->setItem(row, 0, summary);

 for(it = interface->watcher.future().begin();it!=interface->watcher.future().end();it++){
     int row = ui->resultWidget->rowCount();
     ui->resultWidget->insertRow(row);
     QTableWidgetItem* fileName = new QTableWidgetItem((*it).filePath());
     QTableWidgetItem* size = new QTableWidgetItem( QLocale().formattedDataSize((*it).size()) );    
     QTableWidgetItem* date = new QTableWidgetItem((*it).birthTime().date().toString("dd.MM.yyyy"));
     fileName->setFlags(fileName->flags() ^ Qt::ItemIsEditable);
     date->setFlags(date->flags() ^ Qt::ItemIsEditable);
     size->setFlags(date->flags() ^ Qt::ItemIsEditable);
     fileName->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
     date->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
     size->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);

     ui->resultWidget->setItem(row, 0, fileName);
     ui->resultWidget->setItem(row, 1, size);
     ui->resultWidget->setItem(row, 2, date);
 }

}
//____________________________________
void MainWindow::clearTable(){
    while(ui->resultWidget->rowCount()!=0)
        ui->resultWidget->removeRow(0);
}
