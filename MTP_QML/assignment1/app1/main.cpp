#include <QObject>
#include <QCoreApplication>
//#include <QApplication>
#include <QCommandLineParser>
#include <app1.h>
#include <iostream>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCommandLineParser parser;
    QCommandLineOption seedOpt("s", "seed", "QTime::currentTime().minute()", "QTime::currentTime().minute()");
    parser.addOption(seedOpt);
    QCommandLineOption timeOpt("t", "time", "2000", "2000");
    parser.addOption(timeOpt);
    parser.process(a);
    QString seed = parser.value(seedOpt);
    QString time = parser.value(timeOpt);
    if(QStringList(seed)==seedOpt.defaultValues() && QStringList(time)==timeOpt.defaultValues()){
               std::cout<<"Started app1 with default parameters"<<std::endl;
    }
    App1 app(static_cast<quint16>(seed.toInt()),static_cast<quint16>(time.toInt()));
    return a.exec();
}
