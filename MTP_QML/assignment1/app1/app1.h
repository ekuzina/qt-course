#ifndef APP1_H
#define APP1_H

#include <QObject>
#include <QTimer>
#include <QRandomGenerator>
class App1: public QObject
{
    Q_OBJECT
public:
    App1(quint16, quint16);
    ~App1();
private slots:
    void slotTimer();
private:
    QTimer *timer;
    long int* nOut;
    long int* nErr;
    quint16 time;
    QRandomGenerator gen;
};

#endif // APP1_H
