#include "app1.h"
#include <iostream>
#include <QTime>
App1::App1(quint16 seed, quint16 ms):time(ms){
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(slotTimer()));
    nOut = new long int;
    *nOut = 0;
    nErr = new long int;
    *nErr = 0;
    gen.seed(seed);//QTime::currentTime().minute()
    timer->start(time);
}
void App1::slotTimer(){
    if(gen.bounded(0,2)<1)
        std::cout<<"message No "<<++(*nOut)<<" in stdout"<<std::endl;
    else
        std::cerr<<"message No "<<++(*nErr)<<" in stderr"<<std::endl;
    timer->start(time);
}

App1::~App1(){
    delete nOut;
    delete nErr;
}
