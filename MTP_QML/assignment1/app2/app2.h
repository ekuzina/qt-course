#ifndef APP2_H
#define APP2_H

#include <QMainWindow>
#include <QProcess>
#include <QTime>

QT_BEGIN_NAMESPACE
namespace Ui { class App2; }
QT_END_NAMESPACE

class App2 : public QMainWindow
{
    Q_OBJECT

public:
    App2(QWidget *parent = nullptr);
    ~App2();
    QString getFileName()const {return m_fileName;}
    void setFileName(QString& name){m_fileName = name;}

private:
    Ui::App2 *ui;
    QString m_fileName;
    QProcess* m_process;
    QTime* m_stopwatch;
private slots:
    void on_fileButton_clicked();
    void on_runButton_clicked();
   // void on_stopButton_clicked();
    void writeStdout();
    void writeStderr();
    void setStopwatch();
    void finish(int);

};
#endif // APP2_H
