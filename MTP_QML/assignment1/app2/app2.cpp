#include "app2.h"
#include "ui_app2.h"
#include <QFileDialog>
#include <iostream>
App2::App2(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::App2)
{
    ui->setupUi(this);
    m_process = new QProcess(this);
    ui->lineEdit->setText(QString("-t 8000 -s 13"));
    connect(m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(writeStdout()) );
    connect(m_process, SIGNAL(readyReadStandardError()), this, SLOT(writeStderr()) );
    connect(m_process, SIGNAL(started()), this, SLOT(setStopwatch()));
    connect(ui->stopButton, SIGNAL(clicked()), m_process, SLOT(kill()));
    connect(m_process, SIGNAL(finished(int)), this, SLOT(finish(int)));
    //connect(ui->lineEdit, SIGNAL(returnPressed()), this, SLOT(command()) );
    ui->runButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
}

App2::~App2(){
    delete ui;
}

void App2::on_fileButton_clicked(){
    QString name = QFileDialog::getOpenFileName(this,
                                                QString("Select application"),
                                                QString("../"),
                                                QString("Executable program (*.exe)"));
    if (!name.isEmpty()){
        setFileName(name);
        ui->appName->setText(getFileName());
        ui->runButton->setEnabled(true);
    }
}
void App2::on_runButton_clicked(){
    m_process->setProgram(m_fileName);
    QString param = ui->lineEdit->text();
    ui->consoleOutput->append(QString("Starting %1...\n").arg(getFileName()));
    m_process->start();
    ui->fileButton->setEnabled(false);
    if(!param.isEmpty()){
        QStringList paramList = param.split(" ");
        m_process->setArguments(paramList);
    }
    else{
        ui->consoleOutput->append("Started with default parameters.\n");
    }
}
void App2::setStopwatch(){
    m_stopwatch = new QTime;
    m_stopwatch->start();
    ui->runButton->setEnabled(false);
    ui->stopButton->setEnabled(true);
}
void App2::writeStdout(){
    QByteArray text(m_process->readAllStandardOutput());
    QString out("stdout: ");
    out+=text.data();
    ui->consoleOutput->append(out);
}
void App2::writeStderr(){
    QByteArray text(m_process->readAllStandardError());
    QString out("stderr: ");
    out+=text.data();
    ui->consoleOutput->append(out);
}
void App2::finish(int exitCode){
    int procTime = m_stopwatch->elapsed();
    QString finishText = QString("The application finished with the code %1.\n"
                       "Time elapsed: %2 ms\n") .arg(exitCode) .arg(procTime);
    ui->consoleOutput->append(finishText);

    ui->fileButton->setEnabled(true);
    ui->runButton->setEnabled(true);
    ui->stopButton->setEnabled(false);
}
