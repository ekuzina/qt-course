import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.12

import mephi.qt.filter 1.0

ApplicationWindow {
    visible: true
    width: 450; minimumWidth: 450
    height: 320; minimumHeight: 320
    title: qsTr("Median Filter")
    MedianFilter{
        id: filter
    }
    menuBar:
        MenuBar {
        font.pointSize: 10
        Menu {
            font.pointSize: 10
            title: qsTr("&File")
            Action { text: qsTr("&Open origin"); onTriggered: fileOpenDialog.open() }
            MenuSeparator { }
            Action { text: qsTr("&Save result"); onTriggered: saveDir.open() }
            MenuSeparator { }
            Action { text: qsTr("&Quit"); onTriggered: {filter.rmTmp(); Qt.quit()} }
        }
        Menu {
            font.pointSize: 10
            title: qsTr("&Process")
            Action { text: qsTr("Start"); onTriggered: filter.startProcess() }
            Action { text: qsTr("&Stop"); onTriggered: {messageDialog.open(); filter.cancelProcess()} }
       }
    }
    GridLayout{
        columns: 4
        rows: 4
        anchors.fill: parent
        Layout.columnSpan: 10; Layout.rowSpan: 5; Layout.margins: 5
        Layout.alignment: Qt.AlignCenter
        Text {
            id: winLabel
            Layout.column: 0; Layout.row: 0;
            Layout.fillWidth: true; Layout.maximumHeight: 20; Layout.minimumWidth: 100
            Layout.leftMargin: 10
            height: 12
            font.pointSize: 9
            text: qsTr("Filter window size (N*N): ")
        }// Text winLabel

        SpinBox{
            id: winSpinBox
            Layout.column: 1; Layout.row: 0;
            Layout.fillWidth: true
            Layout.maximumHeight: 20; Layout.minimumWidth: 100
            height: 12
            from: 1; to: 99; stepSize: 2
            value: 3
            onValueChanged: filter.setWindowSize(value)
        }// SpinBox winSpinBox

        Text {
            id: threadLabel
            Layout.column: 0; Layout.row: 1;
            Layout.fillWidth: true; Layout.maximumHeight: 20; Layout.minimumWidth: 100
            Layout.leftMargin: 10
            height: 12
            font.pointSize: 9
            text: qsTr("Number of threads: ")
        }// Text threadLabel

        SpinBox{
            id: threadSpinBox
            Layout.column: 1; Layout.row: 1;
            Layout.fillWidth: true; Layout.maximumHeight: 20; Layout.minimumWidth: 100
            height: 20
            from: 1; to: 99; stepSize: 1
            value: 2
            onValueChanged: filter.setThreads(value)
        }// SpinBox threadSpinBox

        Rectangle {
            id: origin
            Layout.column: 0; Layout.row: 2;
            Layout.fillHeight: true; Layout.fillWidth: true;
            Layout.columnSpan: 2;
            Layout.minimumWidth: 200; Layout.minimumHeight: 100
            Layout.alignment: Qt.AlignLeft; Layout.leftMargin: 10
            color: "lightsteelblue"
            border.color: "slategrey"
            Image {
                id: originImg
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                asynchronous: true
                source: Qt.resolvedUrl("pics/noimage")
            }
        }// Image origin

        ProgressBar {
            id: progressBar
            Layout.column: 0; Layout.row: 3;
            Layout.fillHeight: true; Layout.fillWidth: true; Layout.minimumHeight: 15;
            Layout.leftMargin: 10
            Layout.columnSpan: 2
            from: 0; to: 100
            value: filter.progress
        }
        Text {
            id: time
            Layout.column: 0; Layout.row: 4
            Layout.fillHeight: true; Layout.fillWidth: true
            Layout.columnSpan: 2; Layout.leftMargin: 10
            text: qsTr("Process took %1 seconds")
                   .arg( (filter.time>0.) ? filter.time:" ... ")
            font.pointSize: 15
        }
        Button{
            id: fileButton
            Layout.column: 3; Layout.row: 0;
            Layout.fillHeight: true; Layout.fillWidth: true;
            Layout.minimumWidth: 100; Layout.maximumHeight: 20;
            Layout.rightMargin: 10
            height: 12
            text: qsTr("Select image")
            onClicked: fileOpenDialog.open()
        }
        Rectangle {
            id: result
            Layout.column: 2; Layout.row: 2;
            Layout.fillHeight: true; Layout.fillWidth: true; Layout.columnSpan: 2;
            Layout.minimumWidth: 200; Layout.minimumHeight: 100
            Layout.alignment: Qt.AlignRight; Layout.rightMargin: 10

            color: "lightsteelblue"
            border.color: "slategrey"
            Image {
                id: resultImg
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                asynchronous: true
                source: filter.tmpPath == "" ? Qt.resolvedUrl("pics/noimage") :
                    Qt.resolvedUrl(qsTr(Qt.platform.os === "windows" ? "file:///%1": "file://%1")
                                                                  .arg(filter.tmpPath))
            }
        }// Image result

        Button {
            id: saveImg
            Layout.column: 3; Layout.row: 3;
            Layout.fillHeight: true; Layout.fillWidth: true;
            Layout.minimumWidth: 100; Layout.maximumHeight: 20;
            Layout.rightMargin: 10
            height: 12
            text: qsTr("Save image")
            onClicked: saveDir.open()
        }// Button saveImg

        Button{
            id: start
            Layout.column: 2; Layout.row: 4;
            Layout.fillHeight: true; Layout.fillWidth: true;
            Layout.minimumWidth: 100; Layout.maximumHeight: 20;
            height: 12
            text: qsTr("Start")
            onClicked: filter.startProcess()
        }// Button Start
        Button {
            id: cancel
            Layout.column: 3; Layout.row: 4;
            Layout.fillHeight: true; Layout.fillWidth: true;
            Layout.minimumWidth: 100; Layout.maximumHeight: 20;
            Layout.rightMargin: 10
            height: 12
            text: qsTr("Cancel")
            onClicked: {messageDialog.open(); filter.cancelProcess()}
        }// Button cancel

    }// Grid
    FileDialog {
        id: fileOpenDialog
        title: "Select an image file"
        folder: Qt.resolvedUrl(qsTr("file:///%1").arg(picsPath) )
        nameFilters: [
            "Image files (*.png *.jpeg *.jpg)",
        ]
        onAccepted: {
            console.log(folder.toString())
            originImg.source = fileOpenDialog.fileUrl
            var urlString = fileOpenDialog.fileUrl.toString()
            var s
            if (urlString.startsWith("file:///")) {
                var k = urlString.charAt(9) === ':' ? 8 : 7
                s = urlString.substring(k)
            } else {
                s = urlString
            }
            filter.setOrImgPath(decodeURIComponent(s))
            console.log( qsTr("chosen picture: %1").arg(originImg.source.toString()) )
        }
    }// FileDialog fileOpenDialog
    MessageDialog {
        id: messageDialog
        title: "Waiting for finishing, everything is alright"
        text: "Thank you for your patience!"
        onAccepted: close()
    }// MessageDialog messageDialog
    FileDialog{
        id: saveDir
        selectFolder: true
        title: "Select folder"
        folder: Qt.resolvedUrl(qsTr("file:///%1").arg(picsPath) )
        onAccepted: saveDialog.open()
    }// FileDialog saveDir
    Dialog {
        id: saveDialog
        title: "Saving image"
        width: 300
        height: 100
        Text {
            id: saveMessText
            text: qsTr("Insert <name>.<format> to save filtered image.")
            color: "navy"
            width:  parent.width
            height:  parent.height/3
            font.pointSize: 10
        }
        TextInput{
            id: picFormat
            text:"filtered.jpg"
            focus: true
            cursorVisible: true
            font.pointSize: 20
            width:  parent.width
            height:  parent.height/3
            y: saveMessText.height*0.66+10
        }
        standardButtons: StandardButton.Yes
        onYes: {
            var urlString = saveDir.fileUrl.toString()
            var s
            if (urlString.startsWith("file:///")) {
                var k = urlString.charAt(9) === ':' ? 8 : 7
                s = urlString.substring(k)
            } else {
                s = urlString
            }
            filter.saveResult(decodeURIComponent(s)+"/"+picFormat.text)
        }
    }// Dialog saveDialog

    onClosing: filter.rmTmp()
    Component.onCompleted: console.log(picsPath)
}
