#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QQmlContext>
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    // Add plugin
    QDir dir;
    qDebug()<<"create dir:"<< dir.mkpath("./debug/mephi/qt/filter");

    qDebug()<<"remove qmldir:"<<QFile::remove("./debug/mephi/qt/filter/qmldir");
    qDebug()<<"copy qmldir:"<< QFile::copy("../filterPlugin/qmldir", "./debug/mephi/qt/filter/qmldir");

    qDebug()<<"remove ddl:"<<QFile::remove("./debug/mephi/qt/filter/filterPlugind.dll");
    if(!QFile::copy("../filterPlugin/release/filterPlugind.dll", "./debug/mephi/qt/filter/filterPlugind.dll"))
        qDebug()<<"copy ddl:"<<QFile::copy("../filterPlugin/debug/filterPlugind.dll", "./debug/mephi/qt/filter/filterPlugind.dll");
    else
        qDebug()<<"copy ddl: true";
    // Add picture folder
    QDir picsPath(QGuiApplication::applicationDirPath());
    picsPath.cd("../../../pictures");
    engine.rootContext()->setContextProperty("picsPath", picsPath.absolutePath());

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    return app.exec();
}
