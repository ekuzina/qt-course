#include "medianfilter.h"
#include <QtConcurrent/QtConcurrentMap>
#include <QFuture>
#include <QDir>
#include <cmath>
MedianFilter::MedianFilter(QQuickItem *parent):
    QQuickItem(parent), m_watcher(new QFutureWatcher<QImage>),
    m_windowSize(3), m_result(new QImage(4,4,QImage::Format_ARGB32)),
    m_progress(0), m_threads(2), m_time(0), m_tmpPath(QString())
{
    // By default, QQuickItem does not draw anything. If you subclass
    // QQuickItem to create a visual item, you will need to uncomment the
    // following line and re-implement updatePaintNode()

    // setFlag(ItemHasContents, true);
    m_stopclock.start();// initiate stopclock
    connect(m_watcher, SIGNAL(progressValueChanged(int)), this, SLOT(updateProgress(int)));
    connect(m_watcher, SIGNAL(canceled()), this, SLOT(secElapsed()));
    connect(m_watcher, SIGNAL(finished()), this, SLOT(secElapsed()));
}

MedianFilter::~MedianFilter()
{
}
//_____________________________________
int MedianFilter::windowSize() const{return m_windowSize;}
//_____________________________________
int MedianFilter::progress() const{return m_progress;}
//_____________________________________
QString MedianFilter::orImgPath() const{return m_orImgPath;}
//_____________________________________
void MedianFilter::setOrImgPath(QString OrImgPath){
    if (m_orImgPath == OrImgPath)
        return;
    m_orImgPath = OrImgPath;
    emit orImgPathChanged(m_orImgPath);
}
//_____________________________________
void MedianFilter::setWindowSize(int windowSize){
    if (m_windowSize == windowSize)
        return;
    if(windowSize<0) windowSize = 3;
    if(windowSize%2==0) windowSize = windowSize+1;

    m_windowSize = windowSize;
    emit windowSizeChanged(m_windowSize);
}
//_____________________________________
void MedianFilter::setProgress(int progress){
    if (m_progress == progress)
        return;
    m_progress = progress;
    emit progressChanged(m_progress);
}
//_____________________________________
void MedianFilter::setThreads(int threads){
    if (m_threads == threads)
        return;
    if(threads<1) threads =1;
    m_threads = threads;
    emit threadsChanged(m_threads);
}
//_____________________________________
void MedianFilter::setTime(float time){
    if (fabs(m_time-time)<0.0001)
        return;
    m_time = time;
    emit timeChanged(m_time);
}
//_____________________________________
void MedianFilter::setTmpPath(QString tmpPath){
    if (m_tmpPath == tmpPath)
        return;
    m_tmpPath = tmpPath;
    emit tmpPathChanged(m_tmpPath);
}
//_____________________________________
void MedianFilter::resetStopclock(){m_stopclock.restart();}
//_____________________________________
QList<Pixel> MedianFilter::prepareToStart(){
    setProgress(0); setTime(0);
    m_source = new QImage(m_orImgPath);
    if(m_source->isNull()){
        qDebug()<<"[ERROR] There is no image.";
        return QList<Pixel>();
    }
    qDebug()<<"Image is got.";
    setTmpPath("");
    if(m_result) delete m_result;
    m_result = new QImage(m_source->width(),m_source->height(),QImage::Format_ARGB32);
    expandSource();
    return getPoints();
}
//_____________________________________
void MedianFilter::secElapsed(){
    setTime(m_stopclock.elapsed()/1000.);indexRes=0;
    saveTmp();
}
//_____________________________________
int MedianFilter::threads() const{return m_threads;}
//_____________________________________
void MedianFilter::updateProgress(int newValue){
    int newProgress = 100.*newValue / (m_watcher->progressMaximum()-m_watcher->progressMinimum());
    setProgress(newProgress);
}
//_____________________________________
QList<Pixel> MedianFilter::getPoints(){
    QList<Pixel> pixels;
    for(int i=m_windowSize/2;i<m_source->height()+m_windowSize/2;i++){
        for(int j=m_windowSize/2;j<m_source->width()+m_windowSize/2;j++){
            pixels.push_back({QPoint(j,i), m_expanded, m_windowSize, m_result});
        }
    }
    return pixels;
}
//_____________________________________
void MedianFilter::expandSource(){
    int halfWin = m_windowSize/2;
    m_expanded = new QImage(m_source->width()+ 2*halfWin,
                          m_source->height()+ 2*halfWin,
                          QImage::Format_ARGB32);
    qDebug()<<"Start creating expanded image.";
    for (int i=0; i<m_source->height();i++){
        for(int j=0; j<m_source->width();j++){
            m_expanded->setPixelColor(j+halfWin,i+halfWin,m_source->pixelColor(j,i));
        }
    }
    // Set upper lines
    for (int i=0; i<halfWin;i++){
        for(int j=halfWin; j<m_source->width()+halfWin;j++){
            m_expanded->setPixelColor(j,i,
                                    m_source->pixelColor(
                                        j-halfWin,
                                        halfWin));
        }
    }
    // Set bottom lines
    for (int i=m_source->height()+halfWin; i<m_expanded->height();i++){
        for(int j=halfWin; j<m_source->width()+halfWin;j++){
            m_expanded->setPixelColor(j,i,
                                    m_source->pixelColor(
                                        j-halfWin,
                                        m_source->height()-1));
        }
    }
    // Set left colomns
    for (int i=0; i<m_expanded->height();i++){
        for(int j=0; j<halfWin;j++){
            m_expanded->setPixelColor(j,i,m_expanded->pixelColor(halfWin,i));
        }
    }
    // Set right colomns
    for (int i=0; i<m_expanded->height();i++){
        for(int j=m_source->width()+halfWin; j<m_expanded->width();j++){
            m_expanded->setPixelColor(j,i,m_expanded->pixelColor(m_source->width()-1,i));
        }
    }
    qDebug()<<"Finished creating expanded image.";
}
//_____________________________________
void MedianFilter::startProcess(){
    QList<Pixel> list = prepareToStart();
    if(list.isEmpty()) return;

    QThreadPool::globalInstance()->setMaxThreadCount(m_threads);
    qDebug()<<"Max threads: "<<QThreadPool::globalInstance()->maxThreadCount();
    resetStopclock();
    QFuture<QImage> future = QtConcurrent::mappedReduced(list,&filtering,&createImage,
               QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);
    m_watcher->setFuture(future);
}
//_____________________________________
void MedianFilter::cancelProcess(){
    m_watcher->cancel();
    m_watcher->waitForFinished();
}
//_____________________________________
QString MedianFilter::tmpPath() const{return m_tmpPath;}
//_____________________________________
float MedianFilter::time() const{return m_time;}

void MedianFilter::saveResult(QString name){
    qDebug()<<"saved: "<<m_result->save(name)<<name;
}
//_____________________________________
void MedianFilter::saveTmp()
{
    QFileInfo tmpFile("./tmp/filtered.jpg");
    QFileInfo addTmpFile("./tmp/_filtered.jpg");
    QDir tmpDir;
    tmpDir.mkdir("tmp");
    if(tmpFile.exists()){
        m_result->save(addTmpFile.absoluteFilePath());
        setTmpPath(addTmpFile.absoluteFilePath());
        tmpDir.remove(tmpFile.absoluteFilePath());
    }
    else{
        m_result->save(tmpFile.absoluteFilePath());
        setTmpPath(tmpFile.absoluteFilePath());
        tmpDir.remove(addTmpFile.absoluteFilePath());
    }

}
//_____________________________________
void MedianFilter::rmTmp()
{
    QString tmp("./tmp");
    QDir tmpDir(tmp);
    qDebug()<<"remove temporary directory: "<< tmpDir.removeRecursively();
}
//_____________________________________
void convertIndex(int indx, int width, int* x, int* y){
    *x = (indx%width);
    *y = indx/width;
}
//_____________________________________
int convertIndex(int width, int x, int y){
    return y*width+x;
}
//_____________________________________
resColor filtering(const Pixel& pixel){
    int windowSize = pixel.windowSize;
    QImage* expanded = pixel.expanded;
    std::vector<int> red;
    std::vector<int> green;
    std::vector<int> blue;
    for(int i = pixel.pos.y() -(windowSize/2); i<= pixel.pos.y()+(windowSize/2);i++){
        for (int j = pixel.pos.x()-(windowSize/2); j<= pixel.pos.x()+(windowSize/2);j++){
            red.push_back( expanded->pixelColor(j,i).red() );
            green.push_back( expanded->pixelColor(j,i).green() );
            blue.push_back( expanded->pixelColor(j,i).blue() );
        }
    }
    std::sort(red.begin(),red.end());
    std::sort(green.begin(),green.end());
    std::sort(blue.begin(),blue.end());
    // Set median value
    int redValue = red.size()%2!=0 ?  red.at(red.size()/2+1) :
                                      ( red.at(red.size()/2)+red.at(red.size()/2+1) )/2;
    int greenValue = red.size()%2!=0 ?  green.at(green.size()/2+1) :
                                      ( green.at(green.size()/2)+green.at(green.size()/2+1) )/2;
    int blueValue = red.size()%2!=0 ?  blue.at(blue.size()/2+1) :
                                      ( blue.at(blue.size()/2)+blue.at(blue.size()/2+1) )/2;
    int alphaValue = expanded->pixelColor(pixel.pos.x(),pixel.pos.y()).alpha();
    resColor color = resColor(QColor(redValue ,greenValue, blueValue, alphaValue),
                              pixel.result);
    return color;
}
//_____________________________________
void createImage(QImage &image, const resColor& pixel){
    int x; int y;
    convertIndex(indexRes, pixel.second->width(),&x,&y);
    pixel.second->setPixelColor(QPoint(x,y), pixel.first);
    indexRes++;
}
//_____________________________________
