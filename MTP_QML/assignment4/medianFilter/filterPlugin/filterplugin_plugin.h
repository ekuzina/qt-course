#ifndef FILTERPLUGIN_PLUGIN_H
#define FILTERPLUGIN_PLUGIN_H

#include <QQmlExtensionPlugin>

class FilterPluginPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "mephi.qt.filter")

public:
    void registerTypes(const char *uri) override;
};

#endif // FILTERPLUGIN_PLUGIN_H
