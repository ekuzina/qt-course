#include "filterplugin_plugin.h"
#include "medianfilter.h"

#include <qqml.h>

void FilterPluginPlugin::registerTypes(const char *uri)
{
    // @uri mephi.qt.filter
    qmlRegisterType<MedianFilter>(uri, 1, 0, "MedianFilter");
}



