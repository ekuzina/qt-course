#ifndef MEDIANFILTER_H
#define MEDIANFILTER_H

#include <QQuickItem>
#include <QFutureWatcher>
#include <QTime>
#include <QImage>

struct Pixel{
    QPoint pos;
    QImage* expanded;
    int windowSize;
    QImage* result;
};
typedef QPair<QColor, QImage*> resColor;

class MedianFilter : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(MedianFilter)
    Q_PROPERTY(QString orImgPath READ orImgPath WRITE setOrImgPath NOTIFY orImgPathChanged)
    Q_PROPERTY(int windowSize READ windowSize WRITE setWindowSize NOTIFY windowSizeChanged)
//    Q_PROPERTY(QFutureWatcher* Watcher READ Watcher WRITE setWatcher NOTIFY WatcherChanged)
//    Q_PROPERTY(QTime* stopclock READ stopclock WRITE setStopclock NOTIFY stopclockChanged)
//    Q_PROPERTY(QImage Result READ Result WRITE setResult NOTIFY ResultChanged)
    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(int threads READ threads WRITE setThreads NOTIFY threadsChanged)
    Q_PROPERTY(float time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QString tmpPath READ tmpPath WRITE setTmpPath NOTIFY tmpPathChanged)

public:
    explicit MedianFilter(QQuickItem *parent = nullptr);
    ~MedianFilter() override;
    QString orImgPath() const;
    int windowSize() const;
    int progress() const;
    int threads() const;
    float time() const;
    QString tmpPath() const;

    Q_INVOKABLE void saveTmp();
    Q_INVOKABLE void saveResult(QString);
    Q_INVOKABLE void rmTmp();
    Q_INVOKABLE void startProcess();
    Q_INVOKABLE void cancelProcess();


public slots:
    void setOrImgPath(QString OrImgPath);
    void setWindowSize(int windowSize);
    void setProgress(int progress);
    void setThreads(int threads);
    void setTime(float time);

    void setTmpPath(QString tmpPath);

private slots:
    void updateProgress(int);
    void secElapsed();

signals:
    void orImgPathChanged(QString OrImgPath);
    void windowSizeChanged(int windowSize);
    void progressChanged(int progress);
    void threadsChanged(int threads);
    void timeChanged(float time);

    void tmpPathChanged(QString tmpPath);

private:
     QFutureWatcher<QImage>* m_watcher;
     QString m_orImgPath;
     int m_windowSize;
     QTime m_stopclock;
     QImage* m_source;
     QImage* m_expanded;
     QImage* m_result;
     int m_progress;
     int m_threads;
     float m_time;

     void resetStopclock();
     QList<Pixel> prepareToStart();
     QList<Pixel> getPoints();
     void expandSource();

     QString m_tmpPath;
};

static int indexRes = 0;

void convertIndex(int,int,int*,int*);// convert list index i to pair(x,y)
int convertIndex(int,int,int);// convert pair(x,y) to list index i
void createImage(QImage &, const resColor&);// map function
resColor filtering(const Pixel&);// reduce function

#endif // MEDIANFILTER_H
