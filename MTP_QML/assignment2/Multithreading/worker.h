#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QSemaphore>
#include "coloredbuffer.h"
class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(QObject *parent = nullptr);
    void setBuffer(char*);
    void setFileNumBuf(int*);
    void setBufferSize(int);
    void setUsedSem(QSemaphore*);
    void setReadySem(QSemaphore*);
    void setTotalChars(unsigned int*);
    void setProgressValue(int size);
    void readBuffer();
private:
    QSemaphore* usedBytes;
    QSemaphore* readyToChangeBytes;
    unsigned int* m_totalChars;
    char* m_buffer;
    int m_bufferSize;
    int* m_fileNumBuf;
    int m_progressValue;

signals:
    void updBuffer(ColoredBuffer);
    void progressChanged(int);
    void finish();
public slots:
    void startWorking();
};
inline void Worker::setBuffer(char* buf){m_buffer=buf;}
inline void Worker::setFileNumBuf(int* buf){m_fileNumBuf=buf;}
inline void Worker::setBufferSize(int size){m_bufferSize=size;}
inline void Worker::setProgressValue(int size){m_progressValue=size;}
inline void Worker::setUsedSem(QSemaphore *sem){usedBytes=sem;}
inline void Worker::setReadySem(QSemaphore *sem){readyToChangeBytes=sem;}
inline void Worker::setTotalChars(unsigned int* tot){m_totalChars=tot;}
#endif // WORKER_H
