#ifndef PRODUCER_H
#define PRODUCER_H

#include <vector>

#include <QObject>
#include <QFile>
#include <QSemaphore>
//#include <QMetaType>
#include "coloredbuffer.h"
class Producer: public QObject
{
    Q_OBJECT
public:
    Producer();
    ~Producer();
    void setBuffer(char*);
    void setFileNumBuf(int*);
    void setTotalChars(unsigned int*);
    void setBufferSize(int);
    void setFreeSem(QSemaphore*);
    void setReadySem(QSemaphore*);
    void readFiles();
private:
    std::vector<QFile*>m_files;
    std::vector<int>m_fileNum;
    char* m_buffer;
    int* m_fileNumBuf;
    int m_bufferSize;
    unsigned int* m_totalChars;
    QSemaphore* freeBytes;
    QSemaphore* readyToChangeBytes;

    void setFile(QString);
    void setFile(QFile*);
    void removeFiles();

signals:
    void filesReady();
    void updBuffer(ColoredBuffer);
    void finish();
    void filesRead();
    void fileSet(QString);
public slots:
    void takeFileName(QString);
    void resetFiles();
    void produce();
};
inline void Producer::setBuffer(char* buf){m_buffer=buf;}
inline void Producer::setFileNumBuf(int* buf){m_fileNumBuf=buf;}
inline void Producer::setTotalChars(unsigned int* tot){m_totalChars=tot;}
inline void Producer::setBufferSize(int size){m_bufferSize=size;}
inline void Producer::setFreeSem(QSemaphore *sem){freeBytes=sem;}
inline void Producer::setReadySem(QSemaphore *sem){readyToChangeBytes=sem;}
#endif // PRODUCER_H
