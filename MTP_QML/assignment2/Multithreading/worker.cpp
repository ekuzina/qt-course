#include "worker.h"
#include <QDebug>
#include <QApplication>
#include <QThread>
Worker::Worker(QObject *parent) : QObject(parent),
    usedBytes(nullptr), readyToChangeBytes(nullptr),
    m_totalChars(nullptr),
    m_buffer(nullptr), m_bufferSize(0),m_fileNumBuf(nullptr),
    m_progressValue(0)
{

}

void Worker::readBuffer()
{
    qDebug()<<"worker is working in thread: "<< QThread::currentThread();
    unsigned int i=0;
    unsigned int indx=0;
    m_progressValue=0;
    qDebug()<<"worker m_totalChars: "<<*m_totalChars;

    while (i<*m_totalChars) {

        readyToChangeBytes->acquire();
        //qDebug()<<"worker: readyToChange - "<<readyToChangeBytes->available();
        indx %=m_bufferSize;
        QChar bufChar= QChar(m_buffer[indx]);
        unsigned long k=0; unsigned long hundredBillion = 100000001;
        for(;k<hundredBillion;k++){
            if(bufChar.isLower()) bufChar=bufChar.toUpper();
            else if(bufChar.isUpper()) bufChar=bufChar.toLower();
        }
        m_buffer[indx]=static_cast<char>(bufChar.unicode());
        QString ch = QString(m_buffer[indx]);
        ColoredBuffer changes(indx,m_fileNumBuf[indx],m_buffer[indx]);
        emit updBuffer(changes);

        m_progressValue++;
        //additional increment for 'double' chars
        if(ch.contains("\n"))
            m_progressValue++;
        emit progressChanged(m_progressValue);

        qDebug()<<"worker "<<i<<": "<< ch;
        //qDebug()<<"worker: usedBytes - "<<usedBytes->available()+1;
        usedBytes->release();
        indx++; i++;
        if(ch.contains("\n")) i++;
    }

}

void Worker::startWorking()
{
    readBuffer();
    qDebug()<<"worker is finishing";
    emit finish();
}
