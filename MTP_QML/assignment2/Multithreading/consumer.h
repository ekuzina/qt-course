#ifndef CONSUMER_H
#define CONSUMER_H

#include <QObject>
#include <QSemaphore>


class Consumer : public QObject
{
    Q_OBJECT
public:
    explicit Consumer(QObject *parent = nullptr);
    void setBuffer(char*);
    void setFileNumBuf(int*);
    void setBufferSize(int);
    void setFreeSem(QSemaphore*);
    void setUsedSem(QSemaphore*);
    void setTotalChars(unsigned int*);
    void readBuffer();

private:
    QSemaphore* freeBytes;
    QSemaphore* usedBytes;
    char* m_buffer;
    int* m_fileNumBuf;
    int m_bufferSize;
    unsigned int* m_totalChars;

signals:
    void fillWidget(QPair<int,char>);
    void finish();
public slots:
    void startConsuming();
};
inline void Consumer::setBuffer(char* buf){m_buffer=buf;}
inline void Consumer::setFileNumBuf(int* buf){m_fileNumBuf=buf;}
inline void Consumer::setBufferSize(int size){m_bufferSize=size;}
inline void Consumer::setTotalChars(unsigned int* tot){m_totalChars=tot;}
inline void Consumer::setFreeSem(QSemaphore *sem){freeBytes=sem;}
inline void Consumer::setUsedSem(QSemaphore *sem){usedBytes=sem;}
#endif // CONSUMER_H
