#ifndef COLOREDBUFFER_H
#define COLOREDBUFFER_H

#include <QObject>

class ColoredBuffer
{
public:
    ColoredBuffer();
    ColoredBuffer(int,int,char);
    int m_bufPos;
    int m_fileNum;
    char m_bufChar;
};
Q_DECLARE_METATYPE(ColoredBuffer)
#endif // COLOREDBUFFER_H
