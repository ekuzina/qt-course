#include "producer.h"
#include <QRandomGenerator>
#include <QDebug>
#include <QTime>
#include <QThread>
#include <QApplication>
Producer::Producer():m_buffer(nullptr),m_fileNumBuf(nullptr),  m_bufferSize(5),
                     m_totalChars(nullptr), freeBytes(nullptr), readyToChangeBytes(nullptr)
{}

Producer::~Producer()
{
    removeFiles();
}

void Producer::setFile(QString fileName)
{
    QFile* file = new QFile(fileName);
    if (!file->exists())
        delete file;
    else{
        m_files.push_back(file);
        m_fileNum.push_back(m_files.size()-1);
        emit fileSet(fileName);
    }
}

void Producer::removeFiles()
{
    m_fileNum.clear();
    m_fileNum.resize(0);
    std::vector<QFile*>::iterator it;
    for(it=m_files.begin();it!=m_files.end();it++){
        delete  (*it);
    }
    m_files.clear();
    m_files.resize(0);
}

void Producer::readFiles()
{
    qDebug()<<"producer is producing in thread: "<< QThread::currentThread();
    std::vector<QFile*>::iterator it=m_files.begin();
    while(it != m_files.end()){
        if( (! (*it)->exists() ) ||
            (!(*it)->open(QIODevice::ReadOnly | QIODevice::Text)) ){
            qDebug()<<"file does not exist or can not be opened";
            it = m_files.erase(it);
        }
        it++;
    }
    unsigned int totalChars = 0;
    for(it=m_files.begin(); it!=m_files.end();it++){
        totalChars+=static_cast<unsigned int>( (*it)->size() );
    }
    *m_totalChars=totalChars;
    emit filesReady();
    QRandomGenerator rand(QTime::currentTime().msec());
    unsigned int i=0;
    unsigned int indx=0;
    qDebug()<<"producer m_totalChars: "<<totalChars;
    while(i<totalChars){
      //check if eof reached
      for(std::vector<int>::iterator iter=m_fileNum.begin();
          iter != m_fileNum.end();){
          if(m_files.at(*iter)->atEnd()){
              m_files.at(*iter)->close();
              iter = m_fileNum.erase(iter);
          }
          else  {iter++;}
      }
      //choose which file will be read
      if(m_fileNum.size()==0){
        qDebug()<<"Ohh, producer is finishing in loop";
        removeFiles();
        emit finish();
        return;
      }
      int fileNum = m_fileNum.at( rand.bounded(0, m_fileNum.size()) );
      freeBytes->acquire();
      //qDebug()<<"producer: freeBytes - "<<freeBytes->available();
      indx %= m_bufferSize;
      (m_files.at(fileNum))->read(&m_buffer[indx], 1);
      m_fileNumBuf[indx] = fileNum;
      QString ch = QString(m_buffer[indx]);
      qDebug()<<"producer "<<i<<": "<< ch;
     // qDebug()<<"producer: in "<< indx<< " pos " <<
     //           m_buffer[indx]<<" from file "<<  m_fileNumBuf[indx];

      ColoredBuffer changes(indx,fileNum,m_buffer[indx]);
      emit updBuffer(changes);
     // qDebug()<<"producer: readyToChangeBytes - "<<readyToChangeBytes->available()+1;
      readyToChangeBytes->release();
      indx++; i++;
      if(ch.contains("\n")) i++;
    }
    for(std::vector<int>::iterator iter=m_fileNum.begin();
        iter != m_fileNum.end();){
        if(m_files.at(*iter)->atEnd()){
            m_files.at(*iter)->close();
            iter = m_fileNum.erase(iter);
        }
        else  iter++;
    }
    if(m_fileNum.size()==0){
      qDebug()<<"producer is finishing";
      removeFiles();
      emit finish();
      return;
    }
    qDebug()<<"Ohh, producer is finishing out of out";
    emit finish();
    return;
}

void Producer::takeFileName(QString fileName)
{
    setFile(fileName);
}

void Producer::resetFiles()
{
    removeFiles();
}

void Producer::produce()
{
    readFiles();
}


