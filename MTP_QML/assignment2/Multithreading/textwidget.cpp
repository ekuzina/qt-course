#include "textwidget.h"
#include "ui_textwidget.h"

TextWidget::TextWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TextWidget)
{
    ui->setupUi(this);
}

TextWidget::~TextWidget()
{
    delete ui;
}

QString TextWidget::fileName() const
{
    return ui->fileName->text();
}
void TextWidget::setFilename(QString name){
    ui->fileName->setText(name);
}
QString TextWidget::fileText() const
{
    return ui->textEdit->toPlainText();
}
void TextWidget::setFileText(QString text){
    ui->textEdit->setText(text);
}

void TextWidget::appendFileText(QString text)
{
    ui->textEdit->textCursor().insertText(text);
}

void TextWidget::setTextColor(QString c)
{
    ui->textEdit->setTextColor(QColor(c));
}
