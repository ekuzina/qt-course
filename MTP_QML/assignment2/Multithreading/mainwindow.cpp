#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QMetaType>
#include <QDebug>
#include <QCloseEvent>
MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    m_totalChars(0), ui(new Ui::MainWindow),fileClicked(0),
    progressValue(0),m_bufferSize(5)

{
    prodThread = new QThread(this);
    producer = new Producer();
    consThread = new QThread(this);
    consumer= new Consumer();
    workThread = new QThread(this);
    worker= new Worker();

    ui->setupUi(this);
    ui->progressBar->setMaximum(0);
    ui->startButton->setEnabled(false);

    createWidgets();
    //prepare wrappers for characters in textEdit
    colorsWrappers << "<font color=\"green\" >";
    colorsWrappers << "<font color=\"blue\" >";
    colorsWrappers << "<font color=\"red\" >";
    colorsWrappers <<  "</font>";


    connect(this, SIGNAL(getFileName(QString) ),producer, SLOT( takeFileName(QString) ));
    connect(this, SIGNAL(resetFiles() ),producer, SLOT( resetFiles() ));

    connect(producer, SIGNAL(fileSet(QString) ),this, SLOT( setWidgetName(QString) ));

    connect(producer, SIGNAL(filesReady() ),this, SLOT( setTotalChars() ));
    connect(this, SIGNAL(startProduce() ),producer, SLOT( produce() ));
    connect(worker, SIGNAL(progressChanged(int) ),this, SLOT( changeProgressVal(int) ));


    connect(producer, SIGNAL( updBuffer(ColoredBuffer) ),this, SLOT( updBufferShow(ColoredBuffer) ));
    connect(worker, SIGNAL( updBuffer(ColoredBuffer) ),this, SLOT( updBufferShow(ColoredBuffer) ));
    qRegisterMetaType<QPair<int,char>>("QPair<int,char>");
    connect(consumer, SIGNAL( fillWidget(QPair<int,char>) ),this, SLOT( appendWidget(QPair<int,char>) ));


    connect(this, SIGNAL( bufferReady() ),consumer, SLOT( startConsuming() ));
    connect(this, SIGNAL( bufferReady() ),worker, SLOT( startWorking() ));

    connect(producer, SIGNAL( finish() ),this, SLOT( finishProdThread() ));
    connect(worker, SIGNAL( finish() ),this, SLOT(finishWorkThread() ));
    connect(consumer, SIGNAL( finish() ),this, SLOT( finishConsThread() ));


    initApp();

}
void MainWindow::initApp(){
    producer->setFreeSem(&freeBytes);
    producer->setReadySem(&readyToReadBytes);
    producer->setTotalChars(&m_totalChars);
    consumer->setFreeSem(&freeBytes);
    consumer->setUsedSem(&usedBytes);
    consumer->setTotalChars(&m_totalChars);
    worker->setUsedSem(&usedBytes);
    worker->setReadySem(&readyToReadBytes);
    worker->setTotalChars(&m_totalChars);
}

void MainWindow::resetSemaphores()
{
    int available = freeBytes.available();
    freeBytes.acquire(available);
    available = readyToReadBytes.available();
    readyToReadBytes.acquire(available);
    available = usedBytes.available();
    usedBytes.acquire(available);

}

void MainWindow::startThreads()
{
    producer->moveToThread(prodThread);
    prodThread->start();
    worker->moveToThread(workThread);
    workThread->start();
    consumer->moveToThread(consThread);
    consThread->start();
}

void MainWindow::finishProdThread()
{
    prodThread->quit();
    prodThread->wait();
    qDebug()<<usedBytes.available();
}
void MainWindow::finishWorkThread()
{
   workThread->quit();
   workThread->wait();
}
void MainWindow::finishConsThread()
{
    consThread->quit();
    consThread->wait();
    ui->reset1Button->setEnabled(true);
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::closeEvent(QCloseEvent *e)
{
    if(prodThread->isRunning() || workThread->isRunning() || consThread->isRunning()){
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("The applicationis in progress."
                       "\nWait for the end, please.");
        msgBox.exec();
        e->ignore();
        return;
    }
    qDebug()<<"Running: "<<prodThread->isRunning()<<" "<<workThread->isRunning()<<" "
           <<consThread->isRunning();
    resetWidgets();
    QApplication::quit();
    e->accept();
}

QString MainWindow::createBufferShowStr(QStringList *bufferList)
{
    return bufferList->join("");
}

void MainWindow::createWidgets()
{
    m_widgets.push_back(new TextWidget);//file1
    m_widgets.back()->setTextColor(QString("green"));
    m_widgets.push_back(new TextWidget);//file2
    m_widgets.back()->setTextColor(QString("blue"));
    m_widgets.push_back(new TextWidget);//file3
    m_widgets.back()->setTextColor(QString("red"));
    for(int i=0;i<m_widgets.size();i++){
        m_widgets.at(i)->setFilename("file");
    }
}

void MainWindow::setWidgetName(QString fileName)
{
    for(int i=0;i<m_widgets.size();i++){
        if(m_widgets.at(i)->fileName()=="file"){
            m_widgets.at(i)->setFilename(fileName);
            m_widgets.at(i)->show();
            break;
        }
    }
}

void MainWindow::resetWidgets()
{
    for(int i=0;i<m_widgets.size();i++){
        m_widgets.at(i)->setFilename("file");
        m_widgets.at(i)->setFileText("");
        m_widgets.at(i)->hide();
    }
}

void MainWindow::on_file1Button_clicked()
{
    if(!(fileClicked<3)){
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Can't choose more than three files."
                       "\nClick <Reset> to choose other files.");
        msgBox.exec();
        return;
    }
    QString fileName = QFileDialog::getOpenFileName(this,QString("Select file"),QString("../texts"),
                                                    QString("Text Files (*.txt)"));
    if (!fileName.isEmpty()){
        switch (fileClicked) {
            case 0: {
                ui->fileName1->setStyleSheet("color: green;");
                ui->fileName1->setText(fileName);
                break;}
            case 1: {
                ui->fileName2->setStyleSheet("color: blue;");
                ui->fileName2->setText(fileName);
                break;}
            case 2: {
                ui->fileName3->setStyleSheet("color: red;");
                ui->fileName3->setText(fileName);
                break;}
            default: {break;}
        }
        getFileName(fileName);
        fileClicked++;
    }
    if (fileClicked==2) ui->startButton->setEnabled(true);
}

void MainWindow::on_reset1Button_clicked()
{
    emit resetFiles();
    resetWidgets();
    m_totalChars=0;
    worker->setProgressValue(0);
    delete m_buffer;
    delete m_fileNumBuf;
    resetSemaphores();
    ui->fileName1->setText("");
    ui->fileName2->setText("");
    ui->fileName3->setText("");
    fileClicked=0;
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(0);
    ui->textEdit->setText("");
    ui->startButton->setEnabled(false);
}

void MainWindow::on_startButton_clicked()
{
    ui->startButton->setEnabled(false);
    ui->reset1Button->setEnabled(false);
    if(ui->spinBox->value()==0){
        m_bufferSize=5;
        ui->spinBox->setValue(m_bufferSize);
    }
    else m_bufferSize=ui->spinBox->value();
    for(int i=0;i<m_bufferSize;i++){
        bufferShowList << "";
    }
    resetSemaphores();
    freeBytes.release(m_bufferSize);
    m_buffer = new char[m_bufferSize];
    m_fileNumBuf = new int[m_bufferSize];
    producer->setBuffer(m_buffer);
    producer->setFileNumBuf(m_fileNumBuf);
    producer->setBufferSize(m_bufferSize);
    consumer->setBuffer(m_buffer);
    consumer->setFileNumBuf(m_fileNumBuf);
    consumer->setBufferSize(m_bufferSize);
    worker->setBuffer(m_buffer);
    worker->setFileNumBuf(m_fileNumBuf);
    worker->setBufferSize(m_bufferSize);
    startThreads();
    emit startProduce();
}

void MainWindow::setTotalChars()
{
    ui->progressBar->setMaximum(m_totalChars);
    qDebug()<<"main m_totalChars: "<<m_totalChars;
    emit bufferReady();
}

void MainWindow::changeProgressVal(int progressValue)
{
    ui->progressBar->setValue(progressValue);
}

void MainWindow::updBufferShow(ColoredBuffer changes)
{
    QString newChar = colorsWrappers.at(changes.m_fileNum);
    if (changes.m_bufChar=='\n') newChar += "/n";
    else newChar += changes.m_bufChar;
    newChar += colorsWrappers.last();
    bufferShowList.replace( changes.m_bufPos, newChar);
    qDebug()<<changes.m_bufPos<<" "<<newChar;
    ui->textEdit->setText(bufferShowList.join(""));
}

void MainWindow::appendWidget(QPair<int, char>toFill)
{
    m_widgets.at(toFill.first)->appendFileText( QString(toFill.second) );
}
