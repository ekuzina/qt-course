#ifndef TEXTWIDGET_H
#define TEXTWIDGET_H

#include <QWidget>

namespace Ui {
class TextWidget;
}

class TextWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TextWidget(QWidget *parent = nullptr);
    ~TextWidget();
    QString fileName() const;
    void setFilename(QString);
    QString fileText() const;
    void setFileText(QString);
    void appendFileText(QString);
    void setTextColor(QString);

private:
    Ui::TextWidget *ui;
};
#endif // TEXTWIDGET_H
