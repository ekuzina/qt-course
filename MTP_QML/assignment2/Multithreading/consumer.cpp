#include "consumer.h"
#include <QDebug>
#include <QApplication>
#include <QThread>
Consumer::Consumer(QObject *parent) : QObject(parent),
    freeBytes(nullptr), usedBytes(nullptr),
    m_buffer(nullptr), m_fileNumBuf(nullptr), m_bufferSize(0),
    m_totalChars(nullptr)
{
}

void Consumer::readBuffer()
{
    qDebug()<<"consumer is consuming in thread: "<< QThread::currentThread();
    unsigned int i=0;
    unsigned int indx=0;
    qDebug()<<"consumer m_totalChars: "<<*m_totalChars;
    while (i<*m_totalChars) {

        usedBytes->acquire();
       // qDebug()<<"consumer:usedBytes - "<<usedBytes->available();
        indx %=m_bufferSize;
        QPair<int,char> toFill( m_fileNumBuf[indx], m_buffer[indx] );
        emit fillWidget(toFill);
        QString ch = QString(m_buffer[indx]);
        qDebug()<<"consumer "<<i<<": "<< ch;
        //qDebug()<<"consumer:freeBytes - "<<freeBytes->available()+1;
        freeBytes->release();
        indx++; i++;
        if(ch.contains("\n")) i++;
    }
}

void Consumer::startConsuming()
{
    readBuffer();
    qDebug()<<"consumer is finishing";
    emit finish();
}

