#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include "textwidget.h"
#include "producer.h"
#include "consumer.h"
#include "worker.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void closeEvent(QCloseEvent*);
    char* m_buffer;
    int* m_fileNumBuf;
    QSemaphore freeBytes;
    QSemaphore readyToReadBytes;
    QSemaphore usedBytes;

private:
    unsigned int m_totalChars;
    Ui::MainWindow *ui;
    QThread* prodThread;
    Producer* producer;
    QThread* consThread;
    Consumer* consumer;
    QThread* workThread;
    Worker* worker;
    std::vector<TextWidget*> m_widgets;
    int16_t fileClicked;
    int progressValue;
    QStringList colorsWrappers;
    QStringList bufferShowList;
    QString bufferShowStr;
    int m_bufferSize;

    QString createBufferShowStr(QStringList*);
    void createWidgets();
    void resetWidgets();
    void initApp();
    void resetSemaphores();
    void startThreads();

private slots:
    void on_file1Button_clicked();
    void on_reset1Button_clicked();
    void on_startButton_clicked();
    void setTotalChars();
    void changeProgressVal(int);
    void updBufferShow(ColoredBuffer);
    void appendWidget(QPair<int,char>);
public slots:
    void setWidgetName(QString);
    void finishProdThread();
    void finishWorkThread();
    void finishConsThread();
signals:
    void getFileName(QString);
    void resetFiles();
    void startProduce();
    void bufferReady();
};
#endif // MAINWINDOW_H

