#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "albumdialog.h"
#include "artistdialog.h"
#include "ratingdelegate.h"
#include <QFileDialog>
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent,  const QString& fileName)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->treeView);
    setFileName(fileName);
    m_model = new MusicModel();
    ui->treeView->setModel(m_model);
    const int ratingColumn = 2;
    RatingDelegate *delegate = new RatingDelegate();
    ui->treeView->setItemDelegateForColumn(ratingColumn, delegate);
    ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->treeView, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(on_customContextMenuRequested(const QPoint&)));
    ui->treeView->expandAll();
    ui->actionEdit->setEnabled(false);
    ui->actionDelete->setEnabled(false);
    ui->actionAddAlbum->setEnabled(false);
    ui->actionAddSong->setEnabled(false);
    connect(m_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(setModified()));
    connect(ui->treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(enabaledUpdate(QModelIndex)));
    setMinimumSize(QSize(1000,500));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_customContextMenuRequested(const QPoint &pos){
    QModelIndex itemIndx = ui->treeView->indexAt(pos);
    Item* item = static_cast<Item*>(itemIndx.internalPointer());
    if (item){
        QMenu *menu=new QMenu(this);
        menu->addAction(QString("Свойства"), this,SLOT(on_actionEdit_triggered()));
        menu->popup(ui->treeView->viewport()->mapToGlobal(pos));
    }
}

void MainWindow::on_actionAddArtist_triggered()
{
    ArtistDialog *dialog = new ArtistDialog();
    dialog->addArtist(m_model, QModelIndex());
    setModified();
}

void MainWindow::on_actionDelete_triggered()
{
    QModelIndex index = ui->treeView->selectionModel()->currentIndex();
    m_model->removeRow(index.row(), index.parent());
    enabaledUpdate(ui->treeView->selectionModel()->currentIndex());
    setModified();
}

void MainWindow::on_actionEdit_triggered(){
    QModelIndex index = ui->treeView->selectionModel()->currentIndex();
    Item *item=static_cast<Item*>(index.internalPointer());
    if(item->toArtist()){
        ArtistDialog *artist = new ArtistDialog();
        artist->setModel(m_model);
        artist->setModelIndex(index);
        artist->display();
        artist->show();
    }
    else if (item->toAlbum()){
        AlbumDialog *album = new AlbumDialog();
        album->setModel(m_model);
        album->setModelIndex(index);
        album->display();
        album->show();
    }
    else if(item->toSong()){
        QMessageBox msgBox;
        msgBox.setText("Click on a song property twice to edit it.");
        msgBox.exec();}

}

void MainWindow::on_actionAddAlbum_triggered(){
    QModelIndex index = ui->treeView->selectionModel()->currentIndex();
    AlbumDialog* album= new AlbumDialog();
    album->addAlbum(m_model, index);
    setModified();
}

void MainWindow::on_actionAddSong_triggered(){
    QModelIndex parentIndx = ui->treeView->selectionModel()->currentIndex();
    int row = m_model->rowCount(parentIndx);
    bool inserted = m_model->insertRow(row, parentIndx);
    if (inserted){
        QModelIndex index = m_model->index(row,0,parentIndx);
        QVariant var = QVariant(QString("new song"));
        m_model->setData(index, var, Qt::EditRole);
        setModified();
    }
}
bool MainWindow::on_actionSave_triggered(){
    QString fileName;
    if (!currFileName.isEmpty()){ fileName = currFileName;}
    else fileName = QFileDialog::getSaveFileName(this, tr("Save"), tr(".."), tr("*.dat"));
    if (fileName.isEmpty()) return false;
    else
    {
        QFile file(fileName);
        if(file.open(QIODevice::WriteOnly))
        {
            QDataStream out(&file);
            out << *(m_model->root());
            file.close();
            setFileName(fileName);
            setWindowModified(false);
            return true;
        }
    }
    return false;

}
void MainWindow::setModified()
{
    setWindowModified(true);
}

void MainWindow::enabaledUpdate(QModelIndex index)
{
    Item* item=static_cast<Item*>(index.internalPointer());
    if(!item or !index.isValid())
    {
        ui->actionEdit->setEnabled(false);
        ui->actionDelete->setEnabled(false);
        ui->actionAddAlbum->setEnabled(false);
        ui->actionAddSong->setEnabled(false);
        return;
    }
    if(item->toArtist())
    {
        ui->actionEdit->setEnabled(true);
        ui->actionDelete->setEnabled(true);
        ui->actionAddAlbum->setEnabled(true);
        ui->actionAddSong->setEnabled(false);
    }
    if(item->toAlbum())
    {
        ui->actionEdit->setEnabled(true);
        ui->actionDelete->setEnabled(true);
        ui->actionAddAlbum->setEnabled(false);
        ui->actionAddSong->setEnabled(true);
    }
    if(item->toSong())
    {
        ui->actionEdit->setEnabled(true);
        ui->actionDelete->setEnabled(true);
        ui->actionAddAlbum->setEnabled(false);
        ui->actionAddSong->setEnabled(false);
    }
}
void MainWindow::on_actionLoad_triggered(){
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
        QString("Open"),QString(".."), tr("*.dat"));
    if (!fileName.isEmpty())
    {
        QFile file(fileName);
        if(file.open(QIODevice::ReadOnly))
        {
            QDataStream in(&file);
            delete m_model->root();
            setWindowModified(false);
            m_model->setRoot(new Item());
            ui->treeView->reset();
            in >> *(m_model->root());
            file.close();
            ui->actionEdit->setEnabled(false);
            ui->actionDelete->setEnabled(false);
            ui->actionAddAlbum->setEnabled(false);
            ui->actionAddSong->setEnabled(false);
        }
    }
    ui->treeView->reset();
    setFileName(fileName);
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (isWindowModified())
    {
        int n = QMessageBox::warning(this,
                                     "Warning!",
                                     "Треклист изменен.\nСохранить?",
                                     "Да", "Нет", "Отмена", 1, 0);
        if(n!=1)//да или отмена
        {
            if(n==2) e->ignore();
            if(n==0)//да
                this->on_actionSave_triggered() ? e->accept():e->ignore();
        }
        else
            e->accept();
    }
    else
        e->accept();
}

void MainWindow::setFileName(const QString &fileName){
    currFileName = fileName;
    setWindowTitle( QString("%1[*] - %2")
    .arg(currFileName.isNull()?"New tracklist":QFileInfo(currFileName).fileName())
    .arg(QApplication::applicationName()));
}
