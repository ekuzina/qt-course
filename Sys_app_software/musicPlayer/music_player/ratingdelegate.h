#ifndef RATINGDELEGATE_H
#define RATINGDELEGATE_H
#include <QStyledItemDelegate>
#include <QTreeView>
class RatingDelegate : public QStyledItemDelegate
{
public:
    //RatingDelegate();
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
public slots:
    void commitAndCloseEditor();
};

#endif // RATINGDELEGATE_H
