#ifndef ITEM_H
#define ITEM_H
#include <QList>
#include <QPixmap>
#include <QString>
#include <QTime>
#include <QDataStream>
class Artist;
class Album;
class Song;
class Item {
public:
    Item();
    Item(QString);
    virtual ~Item();
    void setName(QString name){m_name=name;}
    QString name() const{return m_name;}
    void setComment(QString comment){m_comment=comment;}
    QString comment() const{return m_comment;}
    Item* parent() const{return m_parent;}
    virtual void setParent(Item*);
    virtual void insertChild(Item*, int);
    Item* takeChild(int);
    virtual void takeChild(Item*);
    Item *childAt(int) const;
    int indexOf(Item*) const;
    int childCount() const;
    virtual Artist *toArtist(){return nullptr;}
    virtual Album *toAlbum(){return nullptr;}
    virtual Song *toSong(){return nullptr;}
    enum Members{ITEM=3, ARTIST=5, AlBUM=6, SONG=5};
protected:
    Item *m_parent;
    QList<Item*> m_children;
    QString m_name;
    QString m_comment;
};

class Artist: public Item
{
public:
    Artist():Item(){}
    Artist(QString name):Item(name){}
    void insertChild(Item*, int);
    void takeChild(Item*);
    void setPhoto(QPixmap photo){m_photo = photo;}
    QPixmap photo() const {return m_photo;}
    void setCountry(QString c){m_country = c;}
    QString country() const {return m_country;}
    Artist* toArtist(){return this;}
    Album* toAlbum(){return nullptr;}
    Song* toSong(){return nullptr;}

    friend QDataStream& operator >>(QDataStream &stream, Artist &artist);
private:
    QPixmap m_photo;
    QString m_country;
};

class Album: public Item
{
public:
    Album();
    Album(QString name);
    void setParent(Item*);
    void insertChild(Item*, int);
    void takeChild(Item*);
    void setCover(QPixmap cover){m_cover = cover;}
    QPixmap cover() const {return m_cover;}
    void setGenre(QString genre){m_genre = genre;}
    const QString genre() const {return m_genre;}
    void setYear(int year) {m_year = year;}
    int year() const {return m_year;}
    Artist* toArtist(){return nullptr;}
    Album* toAlbum(){return this;}
    Song* toSong(){return nullptr;}
    friend QDataStream& operator >>(QDataStream &stream, Album &album);
private:
    int m_year;
    QPixmap m_cover;
    QString m_genre;

};

class Song: public Item
{
public:
    Song();
    Song(QString name);
    void setParent(Item*);
    void insertChild(Item*, int){return;}
    void takeChild(Item*){return;}
    void setTime(QTime time){m_time = time;}
    QTime time() const {return m_time;}
    void setRank(int rank);
    int rank() const {return m_rank;}
    Artist* toArtist(){return nullptr;}
    Album* toAlbum(){return nullptr;}
    Song* toSong(){return this;}
    friend QDataStream& operator >>(QDataStream &stream, Song &song);
private:
    QTime m_time;
    int m_rank;
};

QDataStream& operator <<(QDataStream&stream, const Artist& artist);
QDataStream& operator <<(QDataStream&stream, const Album& album);
QDataStream& operator <<(QDataStream&stream, const Song& song);
#endif // ITEM_H
