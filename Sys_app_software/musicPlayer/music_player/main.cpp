#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <QTreeView>
#include "item.h"
#include "musicmodel.h"
//#include <QAbstractItemModelTester>
#include "ratingdelegate.h"
#include "albumdialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("Music player alpha");
    a.setApplicationVersion("0.1");
    a.setOrganizationName("MephiSoft");
    a.setOrganizationDomain("mephi.ru");
    MainWindow w;
    w.show();
    return a.exec();
}

//useful lines
//      AlbumDialog w;
//    QTreeView *treeView = new QTreeView();
//    const int RatingColumn = 2;
//   // RatingDelegate *delegate = new RatingDelegate(treeView);
//    treeView->setItemDelegateForColumn(RatingColumn, new RatingDelegate);
//    MusicModel model;
//    Artist* artist1 = new Artist(QString("artist1"));
//    artist1->setParent(model.root());
//    w.setModel(&model);
//    w.show();
//    new QAbstractItemModelTester(&model, QAbstractItemModelTester::FailureReportingMode::Warning);
//    treeView->setModel(&model);
//    treeView->expandAll();
//    treeView->show();
//    QFile file("../file.dat");
//    file.open(QIODevice::WriteOnly);
//    QDataStream out(&file);   // we will serialize the data into the file
//    out << *(model.root());
//    file.close();
//    QFile file2("../file.dat");
//    file2.open(QIODevice::ReadOnly);
//    QDataStream in(&file2);    // read the data serialized from the file
//    in >> *parent2;
//    file2.close();
//    qDebug() <<"children" << parent2->childCount();
//qDebug() << parent2->childAt(0)->childCount();
//qDebug() << parent2->childAt(1)->childCount();


//MusicModel model;
//Artist* artist1 = new Artist(QString("artist1"));
//artist1->setParent(model.root());
//Artist* artist2 = new Artist(QString("artist2"));
//artist2->setParent(model.root());
//Artist* artist3 = new Artist(QString("artist3"));
//artist3->setParent(model.root());
//Album* album1 = new Album(QString("album1"));
//album1->setParent(artist2);
//Song* song1 = new Song(QString("song1"));
//song1->setParent(album1);
//song1->setTime(QTime::currentTime());
//Song* song2 = new Song(QString("song2"));
//song2->setParent(album1);
//song2->setTime(QTime::currentTime());
//Song* song3 = new Song(QString("song2"));
//song3->setParent(album1);
//song3->setTime(QTime::currentTime());
//Album* album2 = new Album(QString("album2"));
//album2->setParent(artist2);
//Song* song4 = new Song(QString("song1"));
//song4->setParent(album2);
//song4->setTime(QTime::currentTime());
//Song* song5 = new Song(QString("song2"));
//song5->setParent(album2);
//song5->setTime(QTime::currentTime());
//Song* song6 = new Song(QString("song2"));
//song6->setParent(album2);
//song6->setTime(QTime::currentTime());
//Album* album11 = new Album(QString("album1"));
//album11->setParent(artist1);
//Song* song7 = new Song(QString("song1"));
//song7->setParent(album11);
//song7->setTime(QTime::currentTime());
//Song* song8 = new Song(QString("song2"));
//song8->setParent(album11);
//song8->setTime(QTime::currentTime());
//Song* song9 = new Song(QString("song2"));
//song9->setParent(album11);
//song9->setTime(QTime::currentTime());
//Album* album32 = new Album(QString("album2"));
//album2->setParent(artist3);
//Song* song10 = new Song(QString("song1"));
//song10->setParent(album32);
//song10->setTime(QTime::currentTime());
//Song* song11 = new Song(QString("song2"));
//song11->setParent(album32);
//song11->setTime(QTime::currentTime());
//Album* album4 = new Album(QString("album4"));
//album4->setParent(artist2);
//Song* song12 = new Song(QString("song1"));
//song12->setParent(album4);
//song12->setTime(QTime::currentTime());
//Song* song13 = new Song(QString("song2"));
//song13->setParent(album4);
//song13->setTime(QTime::currentTime());
//Song* song14 = new Song(QString("song2"));
//song14->setParent(album4);
//song14->setTime(QTime::currentTime());
//QFile file("../file.dat");
//file.open(QIODevice::WriteOnly);
//QDataStream out(&file);   // we will serialize the data into the file
//out << *(model.root());
//file.close();
