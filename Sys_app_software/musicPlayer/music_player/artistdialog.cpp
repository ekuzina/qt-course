#include "artistdialog.h"
#include "ui_artistdialog.h"
#include <QFileDialog>
#include <QMessageBox>
ArtistDialog::ArtistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ArtistDialog)
{
    ui->setupUi(this);
    setSizeGripEnabled(true);
    setMinimumSize(640,400);
    //connect(ui->coverButton, SIGNAL(clicked()), SLOT(on_coverButton_clicked()));
    //because of this damn line I lost scores for previous works...
    m_mapper.setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
}

ArtistDialog::~ArtistDialog()
{
    delete ui;
}

void ArtistDialog::on_photoButton_clicked(){
    QString fileName = QFileDialog::getOpenFileName(this,QString("Select image"),QString("../music_player/pics"),
                                                QString("Image Files (*.png *.jpg *.bmp)"));
    if (fileName.isEmpty()) return;
    if (!photo.load(fileName)){
        QMessageBox::warning(this, tr("Open Image"),
                             tr("The image is wrong. Can't load it."), QMessageBox::Close);
        return;
    }
    ui->picLabel->setPixmap(photo.scaled(200,200));
}
void ArtistDialog::setModel(MusicModel* model){
        m_mapper.setModel(model);
}

void ArtistDialog::setModelIndex(const QModelIndex &index){
    QModelIndex parentIndx = index.parent();
    if(parentIndx.isValid()) return;//root is not valid
    m_mapper.setRootIndex(parentIndx);
    m_mapper.setCurrentModelIndex(index);

    m_mapper.addMapping(ui->nameEdit,0);
    m_mapper.addMapping(ui->countryEdit,1);
    m_mapper.addMapping(ui->picLabel,2, "pixmap");
    m_mapper.addMapping(ui->comment,3);
}

void ArtistDialog::accept(){
    m_mapper.submit();
    QDialog::accept();
}

bool ArtistDialog::addArtist(MusicModel* model, const QModelIndex &parent){
    setModel(model);
    int row = model->rowCount(parent);
    if(!model->insertRow(row, parent)) return false;
    QModelIndex index = model->index(row, 0, parent);
    setModelIndex(index);
    if(!exec()){
        model->removeRow(row, parent);
        return false;
    }
    return true;
}
void ArtistDialog::display(){
    QModelIndex parentIndx = m_mapper.rootIndex();
    int row = m_mapper.currentIndex();
    QModelIndex index = m_mapper.model()->index(row,0,parentIndx);
    Artist* artist = static_cast<Artist*>(index.internalPointer());
    ui->nameEdit->setText(artist->name());
    ui->picLabel->setPixmap(artist->photo());
    ui->countryEdit->setText(artist->country());
    ui->comment->setPlainText(artist->comment());
}
