#include "albumdialog.h"
#include "ui_albumdialog.h"
#include <QFileDialog>
#include <QMessageBox>
AlbumDialog::AlbumDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlbumDialog)
{
    ui->setupUi(this);
    setSizeGripEnabled(true);
    setMinimumSize(640,400);
    m_mapper.setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
    //connect(ui->coverButton, SIGNAL(clicked()), SLOT(on_coverButton_clicked()));
    //because of this damn line I lost scores for previous works...
}

AlbumDialog::~AlbumDialog()
{
    delete ui;
}

void AlbumDialog::on_coverButton_clicked(){
    QString fileName = QFileDialog::getOpenFileName(this,QString("Select image"),QString("../music_player/pics"),
                                                QString("Image Files (*.png *.jpg *.bmp)"));
    if (fileName.isEmpty()) return;
    if (!cover.load(fileName)){
        QMessageBox::warning(this, tr("Open Image"),
                             tr("The image is wrong. Can't load it."), QMessageBox::Close);
        return;
    }
    ui->picLabel->setPixmap(cover.scaled(200,200));
}
void AlbumDialog::setModel(MusicModel* model){
        m_mapper.setModel(model);
}

void AlbumDialog::setModelIndex(const QModelIndex &index){
    QModelIndex parentIndx = index.parent();
    Item* parent = static_cast<Item*>(parentIndx.internalPointer());
    if(!parent->toArtist()) return;
    ui->artistName->setText(parent->name());
    m_mapper.setRootIndex(parentIndx);
    m_mapper.setCurrentModelIndex(index);

    m_mapper.addMapping(ui->nameEdit,0);
    m_mapper.addMapping(ui->genreEdit,1);
    m_mapper.addMapping(ui->yearEdit,2);
    m_mapper.addMapping(ui->comment,3);
    m_mapper.addMapping(ui->picLabel,4, "pixmap");
}

void AlbumDialog::accept(){
    m_mapper.submit();
    QDialog::accept();
}

bool AlbumDialog::addAlbum(MusicModel* model, const QModelIndex &parent){
    setModel(model);
    int row = model->rowCount(parent);
    if(!model->insertRow(row, parent)) return false;
    QModelIndex index = model->index(row, 0, parent);
    setModelIndex(index);
    if(!exec()){
        model->removeRow(row, parent);
        return false;
    }
    return true;
}
void AlbumDialog::display(){
    QModelIndex parentIndx = m_mapper.rootIndex();
    int row = m_mapper.currentIndex();
    QModelIndex index = m_mapper.model()->index(row,0,parentIndx);
    Album* album = static_cast<Album*>(index.internalPointer());
    ui->nameEdit->setText(album->name());
    ui->picLabel->setPixmap(album->cover());
    ui->yearEdit->setValue(album->year());
    ui->genreEdit->setText(album->genre());
    ui->comment->setPlainText(album->comment());
}
