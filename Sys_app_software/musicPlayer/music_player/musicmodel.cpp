#include "musicmodel.h"
MusicModel::MusicModel():
    m_root(new Item(QString("root"))){
}

QModelIndex	MusicModel::index(int row, int column, const QModelIndex &parentIndx = QModelIndex())const {
    Item* parent = m_root;
    if(parentIndx.isValid())
        parent = static_cast<Item*>(parentIndx.internalPointer());        
    if(parent->childAt(row) && columnCount(parentIndx)>column)
            return createIndex(row, column, parent->childAt(row));
    return QModelIndex(); //empty model index
}
QModelIndex MusicModel::parent(const QModelIndex& childIndx) const{
    if(!childIndx.isValid()) return QModelIndex();
    Item* child = static_cast<Item*>(childIndx.internalPointer());
    Item *parent = child->parent();
    if(child == m_root || parent == m_root) return QModelIndex();
    if(!parent) return QModelIndex();
    Item *grandpa = parent->parent();
    if(!grandpa) return QModelIndex();
    return createIndex(grandpa->indexOf(parent),0,parent);
}

int MusicModel::rowCount(const QModelIndex &parentIndx = QModelIndex()) const{
    if(!parentIndx.isValid()) return m_root->childCount();
    Item* parent = static_cast<Item*>(parentIndx.internalPointer());
    return parent->childCount();
}

int MusicModel::columnCount(const QModelIndex& parentIndx = QModelIndex()) const{
    if(!parentIndx.isValid()) //m_root
        return Item::Members::ARTIST-1;
    Item* parent = static_cast<Item*>(parentIndx.internalPointer());
    if(parent->toArtist())
        return Item::Members::AlBUM-1;
    if(parent->toAlbum())
        return Item::Members::SONG-1;
    return 0;//if(parent->toSong())
}

QVariant MusicModel::data(const QModelIndex& index, int role = Qt::DisplayRole) const{
    if(!(index.isValid())) return QVariant();
    Item* item = static_cast<Item*>(index.internalPointer());
    if(!item) return QVariant();
    switch (role){
    case (Qt::DisplayRole):{
        if (index.column()==0) {
            return item->name();
        }
        if (index.column()==3) {
            return item->comment();
        }
        if(item->toSong()){
            Song* song = item->toSong();
            switch (index.column()) {
            case 1:
                return song->time();
            case 2:
                return song->rank();
            }
         }
        break;
    }
    case (Qt::EditRole):{
        if (index.column()==0) return item->name();
        else if (index.column()==3) return item->comment();
        else{
            if(item->toArtist()){
                Artist* artist = item->toArtist();
                switch(index.column()){
                case 1:
                    return artist->country();
                case 2:
                    return artist->photo();
                }
            }
             if(item->toAlbum()){
                 Album* album = item->toAlbum();
                 switch(index.column()){
                 case 1:
                     return album->genre();
                 case 2:
                     return album->year();
                 case 4:
                    return album->cover();
                 }
             }
             if(item->toSong()){
                 Song* song = item->toSong();
                 switch(index.column()){
                 case 1:
                     return song->time();
                 case 2:
                     return song->rank();
                 }
             }
        }
        break;
    }
    default: return QVariant();
    }
    return QVariant();
}

QVariant MusicModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString("Название");
        case 1:
            return QString("Длительность");
        case 2:
            return QString("Рейтинг");
        case 3:
            return QString("Комментарий");
        }
    }
    return QVariant();
}

Qt::ItemFlags MusicModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
            return Qt::ItemIsEnabled;
    Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
    return defaultFlags;
}
bool MusicModel::setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole){
   if(!(index.isValid() && (role == Qt::EditRole || role == Qt::DisplayRole) )) return false;
   Item* item = static_cast<Item*>(index.internalPointer());
   if (!item) return false;
   if (index.column()==0) item->setName(value.toString());
   else if (index.column()==3) item->setComment(value.toString());
   else{
       if(item->toArtist()){
           Artist* artist = item->toArtist();
           switch(index.column()){
           case 1:
               artist->setCountry(value.toString());
               break;
           case 2:
               artist->setPhoto(value.value<QPixmap>());
               break;
           }
       }
        if(item->toAlbum()){
            Album* album = item->toAlbum();
            switch(index.column()){
            case 1:
                album->setGenre(value.toString());
                break;
            case 2:
                album->setYear(value.toInt());
                break;
            case 4:
                album->setCover(value.value<QPixmap>());
                break;
            }
        }
        if(item->toSong()){
            Song* song = item->toSong();
            switch(index.column()){
            case 1:
                song->setTime(value.toTime());
                break;
            case 2:
                song->setRank(value.toInt());
                break;
            }
        }
   }
   emit dataChanged(index, index);
   return true;
}
bool MusicModel::insertRows(int row, int count, const QModelIndex &parentIndx = QModelIndex()){
    Item* parent = m_root;
    if(parentIndx.isValid())
        parent = static_cast<Item*>(parentIndx.internalPointer());
    if(!parent) return false;
    Item* newRow=nullptr;
    if(parent==m_root) newRow = new Artist();
    if(parent->toArtist()) newRow = new Album();
    if(parent->toAlbum()) newRow = new Song();
    beginInsertRows(parentIndx, row, row+count-1);
    for(int i=0;i<count;i++){
        parent->insertChild(newRow, row+i);
    }
    endInsertRows();
    return true;
}
bool MusicModel::removeRows(int row, int count, const QModelIndex &parentIndx = QModelIndex()){
    Item* parent = m_root;
    if(parentIndx.isValid())
        parent = static_cast<Item*>(parentIndx.internalPointer());
    if(!parent) return false;
    beginRemoveRows(parentIndx, row, row+count-1);
    for(int i=0;i<count;i++){
        parent->takeChild(row);
    }
    endRemoveRows();
    return true;
}

QDataStream& operator<< (QDataStream &stream, Item &root){
    int cnt = root.childCount();
    stream << cnt;
    for(int i=0; i < cnt; i++){
        Artist const* artist = root.childAt(i)->toArtist();
        if (artist) stream << *artist;
    }
    return stream;
}

QDataStream& operator>> (QDataStream &stream, Item &root){
    int artistNum;
    Artist *artist;
    stream >> artistNum;
    for (int i =0; i<artistNum; i++){
        artist = new Artist();
        stream >> *artist;
        root.insertChild(artist,i);
    }
    return stream;
}
