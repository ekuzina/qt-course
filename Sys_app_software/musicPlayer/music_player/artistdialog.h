#ifndef ARTISTDIALOG_H
#define ARTISTDIALOG_H

#include <QDialog>
#include <QDialog>
#include "musicmodel.h"
#include <QDataWidgetMapper>

namespace Ui {
class ArtistDialog;
}

class ArtistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ArtistDialog(QWidget *parent = nullptr);
    ~ArtistDialog();
    void setModel(MusicModel*);
    void setModelIndex(const QModelIndex &);
    void accept();
    bool addArtist(MusicModel *model, const QModelIndex &parent);
    void display();
private slots:
    void on_photoButton_clicked();
private:
    Ui::ArtistDialog *ui;
    QPixmap photo;
    QDataWidgetMapper m_mapper;
};
#endif // ARTISTDIALOG_H
