#ifndef RANKEDITOR_H
#define RANKEDITOR_H

#include <QWidget>
#include <QMouseEvent>
#include <QPaintEvent>
class RankEditor : public QWidget
{
    Q_OBJECT
public:
    explicit RankEditor(QWidget *parent = nullptr);
    QSize sizeHint() const override {return QSize(100,20);}
    void setRank(int rank);
    int rank() { return m_rank; }

signals:
    void editingFinished();

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    int circleAtPosition(int x) const;
    int m_rank;
};

#endif // RANKEDITOR_H
