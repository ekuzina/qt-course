#ifndef MUSICMODEL_H
#define MUSICMODEL_H
#include "item.h"
#include <QAbstractItemModel>
#include <QModelIndex>

class MusicModel : public QAbstractItemModel {
public:
    explicit MusicModel();
    QModelIndex	index(int, int, const QModelIndex&) const;
    QModelIndex parent(const QModelIndex&) const;
    int rowCount(const QModelIndex&) const;
    int columnCount(const QModelIndex&) const; //{return (int)colNum;}
    QVariant data(const QModelIndex&, int) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Item* root(){return m_root;}
    void setRoot(Item* root){m_root=root;}
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    bool insertRows(int row, int count, const QModelIndex &parentIndx);
    bool removeRows(int row, int count, const QModelIndex &parentIndx);
private:
    Item* m_root=nullptr;
};
QDataStream& operator<< (QDataStream &stream, Item &root);
QDataStream& operator>> (QDataStream &stream, Item &root);

#endif // MUSICMODEL_H
