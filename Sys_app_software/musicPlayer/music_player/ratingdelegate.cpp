#include "ratingdelegate.h"
#include "item.h"
#include <QPainter>
#include "rankeditor.h"
QSize RatingDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const{
    return QSize(100,20);
}
void RatingDelegate::paint(QPainter* rankPainter,
                           const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if (!index.isValid()) return;
    if(!index.data().canConvert<int>()) return QStyledItemDelegate::paint(rankPainter, option, index);
    Item* item = static_cast<Item*>(index.internalPointer());
    if(!item->parent()->toAlbum()) return QStyledItemDelegate::paint(rankPainter, option, index);
    if(!item->toSong()) return QStyledItemDelegate::paint(rankPainter, option, index);
    if(index.column() == 2){
        int rank = qvariant_cast<int>(index.data());
        QPoint topLeft = option.rect.topLeft();
        rankPainter->setBrush(Qt::yellow);
        rankPainter->setPen(Qt::black);
        rankPainter->save();
        for (int i=0;i<rank;i++){
            rankPainter->drawEllipse(topLeft.x()+2+((16+4)*i),topLeft.y()+2,16,16);
        }
        rankPainter->setBrush(Qt::NoBrush);
        for (int i=rank;i<5;i++){
            rankPainter->drawEllipse(topLeft.x()+2+((16+4)*i),topLeft.y()+2,16,16);
        }
        rankPainter->restore();
    }
}

QWidget* RatingDelegate::createEditor(QWidget* parent,
                                      const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if(!index.isValid()) return QStyledItemDelegate::createEditor(parent, option, index);
    Item* item = static_cast<Item*>(index.internalPointer());
    if(!item->parent()->toAlbum()) return QStyledItemDelegate::createEditor(parent, option, index);
    if(!item->toSong()) return QStyledItemDelegate::createEditor(parent, option, index);
    RankEditor *editor = new RankEditor(parent);
    connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return editor;
}
void RatingDelegate::setEditorData(QWidget* editor, const QModelIndex &index) const{
    if(!index.isValid()) return QStyledItemDelegate::setEditorData(editor, index);
    Item* item = static_cast<Item *>(index.internalPointer());
    if(!item->parent()->toAlbum()) return QStyledItemDelegate::setEditorData(editor, index);
    if(!item->toSong()) return QStyledItemDelegate::setEditorData(editor, index);
    int rank=0;
    if(index.column() == 2){
        rank = qvariant_cast<int>(index.data());
        RankEditor* editorRank = qobject_cast<RankEditor*>(editor);
        editorRank->setRank(rank);
    }
    else
        QStyledItemDelegate::setEditorData(editor, index);
}
void RatingDelegate::setModelData(QWidget* editor,
                                  QAbstractItemModel* model, const QModelIndex &index) const{
    if(!index.isValid()) return QStyledItemDelegate::setModelData(editor, model, index);
    Item* item = static_cast<Item*>(index.internalPointer());
    if(!item->parent()->toAlbum()) return QStyledItemDelegate::setModelData(editor, model, index);
    if(!item->toSong()) return QStyledItemDelegate::setModelData(editor, model, index);
    if(index.column() == 2){
        RankEditor* editorRank = qobject_cast<RankEditor*>(editor);
        item->toSong()->setRank(editorRank->rank());
        model->setData(index, QVariant::fromValue(editorRank->rank()));
    }
    else
    QStyledItemDelegate::setModelData(editor, model, index);
}
void RatingDelegate::commitAndCloseEditor(){
    RankEditor *editor = qobject_cast<RankEditor*>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
}
