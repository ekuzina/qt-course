#include "item.h"

Item::Item():
    m_parent(nullptr), m_name("New"), m_comment("")
{
    if(!m_children.isEmpty()){
        m_children.clear();
    }
}
Item::Item(QString name):
    m_parent(nullptr), m_name(name), m_comment("")
{
    if(!m_children.isEmpty()){
        m_children.clear();
    }
}
Album::Album():Item(){
    m_year=1970;}
Album::Album(QString name):Item(name){
    m_year=1970;}
Song::Song():Item(){
    m_rank=0;}
Song::Song(QString name):Item(name){
    m_rank=0;}
Item::~Item()
{
    setParent(nullptr);
    for(QList<Item*>::Iterator it=m_children.begin(); it!=m_children.end(); it++){
        delete (*it);
    }
    m_children.clear();
}
void Item::insertChild(Item* child, int position = -1){
    if(!child) return;
    if(m_children.contains(child)){
        child->setParent(this);
        return;
    }
    if(position <0 || position > m_children.size()){
        m_children.push_back(child);
    }
    else{
        m_children.insert(position,child);
    }
    child->setParent(this);
}
//--------------
void Artist::insertChild(Item* child, int position = -1){
    if(!child) return;
    Album* album = child->toAlbum();
    if(!album) return;
    if(m_children.contains(album)){
        album->setParent(this);
        return;
    }
 //   if(position <0 || position > m_children.size()){
 //       m_children.push_back(album);
 //   }
 //   else{
        m_children.insert(position,album);
 //   }
    album->setParent(this);
}
void Album::insertChild(Item* child, int position = -1){
    if(!child) return;
    Song* song = child->toSong();
    if(!song) return;
    if(m_children.contains(song)){
        song->setParent(this);
        return;
    }
//    if(position <0 || position > m_children.size()){
//        m_children.push_back(song);
//    }
//    else{
        m_children.insert(position,song);
//    }
    song->setParent(this);
}
//--------------
Item* Item::takeChild(int position){
    if(position<0 || position>=m_children.size()){
        return nullptr;
    }
    Item* child = m_children.takeAt(position);
    child->setParent(nullptr);
    return  child;
}

void Item::takeChild(Item* child){
    if(!child){
        return;
    }
    bool find = m_children.removeOne(child);
    if (find) child->setParent(nullptr);
}
//--------------
void Artist::takeChild(Item* child){
    if(!child) return;
    Album* album = child->toAlbum();
    if(!album){
        return;
    }
    bool find = m_children.removeOne(album);
    if (find) album->setParent(nullptr);
}
void Album::takeChild(Item* child){
    if(!child) return;
    Song* song = child->toSong();
    if(!song){
        return;
    }
    bool find = m_children.removeOne(song);
    if (find) song->setParent(nullptr);
}
//--------------
void Item::setParent(Item* parent){
    if(m_parent==parent) return;
    if(parent){
        if(m_parent){
            m_parent->takeChild(this);
            m_parent=parent;
            m_parent->insertChild(this,-1);
        }
        else{
            m_parent=parent;
            m_parent->insertChild(this,-1);
        }
    }
    else{//parent=null
        if (!m_parent) return;
        m_parent->takeChild(this);
        m_parent=nullptr;
    }
}
//--------------
void Album::setParent(Item* parent){
    Artist* artist = nullptr;
    if(parent){
        artist = parent->toArtist();
        if(!artist) return;
    }
    if(m_parent==artist) return;
    if(artist){
        if(m_parent){
            m_parent->takeChild(this);
            m_parent=artist;
            m_parent->insertChild(this,-1);
        }
        else{
            m_parent=artist;
            m_parent->insertChild(this,-1);
        }
    }
    else{//parent=null
        if (!m_parent) return;
        m_parent->takeChild(this);
        m_parent=nullptr;
    }
}
void Song::setParent(Item* parent){
    Album* album = nullptr;
    if(parent){
        album = parent->toAlbum();
        if(!album) return;
    }
    if(m_parent==album) return;
    if(album){
        if(m_parent){
            m_parent->takeChild(this);
            m_parent=album;
            m_parent->insertChild(this,-1);
        }
        else{
            m_parent=album;
            m_parent->insertChild(this,-1);
        }
    }
    else{//parent=null
        if (!m_parent) return;
        m_parent->takeChild(this);
        m_parent=nullptr;
    }
}
//--------------
Item* Item::childAt(int i) const{
    if(i<0 || i>=m_children.size() || childCount()<1)
        return nullptr;
    return m_children.at(i);
}

int Item::indexOf(Item* child) const{
    bool find =false;
    int i;
    for(i=0; i<m_children.size(); i++){
        if(m_children.at(i)==child){find=true;break;}
    }
    if(find){
        return i;
    }
    return -1;
}

int Item::childCount() const{
   if (m_children.isEmpty()) return 0;
   return m_children.size();
}

void Song::setRank(int rank){
    if(rank<0) m_rank=0;
    else if(rank>5) m_rank=5;
    else m_rank=rank;
}

QDataStream& operator<<(QDataStream &stream, const Artist &artist) {
//    stream << "ARTIST";
    stream << artist.name() << artist.comment()
           << artist.photo() << artist.country();
    // serialize children
    int cnt = artist.childCount();
    stream << cnt;
    for(int i=0; i<cnt; ++i){
        if(artist.childAt(i)){
            Album *album = artist.childAt(i)->toAlbum();
            if(album)
                stream << *album;
        }
    }
    return stream;
}

QDataStream& operator<<(QDataStream &stream, const Album &album) {
//    stream << "ALBUM";
    stream << album.name() << album.comment()
           << album.year() << album.cover()
           << album.genre();
    // serialize children
    int cnt = album.childCount();
    stream << cnt;
    for(int i=0; i<cnt; ++i){
        if(album.childAt(i)){
            Song *song = album.childAt(i)->toSong();
            if(song)
                stream << *song;
        }
    }
    return stream;
}

QDataStream& operator<<(QDataStream &stream, const Song &song) {
 //   stream << "SONG";
    stream << song.name() << song.comment()
           << song.time() << song.rank();
    return stream;
}
//---------------
QDataStream &operator >>(QDataStream &stream, Artist &artist)
{
//    QString title;
    int albNum;
    Album *album;
//    stream >> title;
//    if(!title.contains("ARTIST")) return stream;
    stream >> artist.m_name >> artist.m_comment
           >> artist.m_photo >> artist.m_country;
    stream >> albNum;
    for (int i =0; i<albNum; i++)
    {
        album = new Album();
        stream >> *album;
        artist.insertChild(album,i);
    }
    return stream;
}

QDataStream& operator >>(QDataStream &stream, Album &album) {
//    QString title;
    int songNum;
    Song *song;
//    stream >> title;
//    if(!title.contains("ALBUM")) return stream;
    stream >> album.m_name >> album.m_comment
           >> album.m_year >> album.m_cover
           >> album.m_genre;
    stream >> songNum;
    for (int i =0; i<songNum; i++)
    {
        song = new Song();
        stream >> *song;
        album.insertChild(song,i);
    }
    return stream;
}

QDataStream& operator >>(QDataStream &stream, Song &song) {
//    QString title;
//    stream >> title;
//    if(!title.contains("SONG")) return stream;
    stream >> song.m_name >> song.m_comment
           >> song.m_time >> song.m_rank;
    return stream;
}
