#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "musicmodel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, const QString& fileName=QString());
    ~MainWindow();
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;
    QString currFileName;
    MusicModel *m_model;
    void setFileName(const QString &fileName);
    Item* root(MusicModel*);
public slots:
    void setModified();
    void enabaledUpdate(QModelIndex index);
    void on_customContextMenuRequested(const QPoint &pos);
private slots:
    void on_actionAddArtist_triggered();
    void on_actionDelete_triggered();
    void on_actionEdit_triggered();
    void on_actionAddAlbum_triggered();
    void on_actionAddSong_triggered();
    bool on_actionSave_triggered();
    void on_actionLoad_triggered();
};
#endif // MAINWINDOW_H
