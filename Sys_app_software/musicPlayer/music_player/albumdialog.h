#ifndef ALBUMDIALOG_H
#define ALBUMDIALOG_H

#include <QDialog>
#include "musicmodel.h"
#include <QDataWidgetMapper>
namespace Ui {
class AlbumDialog;
}

class AlbumDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AlbumDialog(QWidget *parent = nullptr);
    ~AlbumDialog();
    void setModel(MusicModel*);
    void setModelIndex(const QModelIndex &);
    void accept();
    bool addAlbum(MusicModel *model, const QModelIndex &parent);
    void display();
private slots:
    void on_coverButton_clicked();
private:
    Ui::AlbumDialog *ui;
    QPixmap cover;
    QDataWidgetMapper m_mapper;
};

#endif // ALBUMDIALOG_H
