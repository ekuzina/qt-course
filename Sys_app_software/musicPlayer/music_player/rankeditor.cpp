#include "rankeditor.h"
#include <QPainter>
RankEditor::RankEditor(QWidget *parent) : QWidget(parent){
    setMouseTracking(true);
    setAutoFillBackground(true);
}
void RankEditor::setRank(int rank){
    if(rank<0) m_rank=0;
    else if(rank>5) m_rank=5;
    else m_rank=rank;
}

void RankEditor::paintEvent(QPaintEvent *){
    QPainter rankPainter(this);
    QPoint topLeft(0,0);
    rankPainter.setBrush(Qt::yellow);
    rankPainter.setPen(Qt::black);
    rankPainter.save();
    int i=0;
    for (i=0;i<m_rank;i++){
        rankPainter.drawEllipse(topLeft.x()+2+((16+4)*i),topLeft.y()+2,16,16);
    }
    rankPainter.setBrush(Qt::NoBrush);
    for (i=m_rank;i<5;i++){
        rankPainter.drawEllipse(topLeft.x()+2+((16+4)*i),topLeft.y()+2,16,16);
    }
    rankPainter.restore();
}

void RankEditor::mousePressEvent(QMouseEvent *event){
    int rank = circleAtPosition(event->x());
    if (rank != m_rank && rank != -1) {
        setRank(rank);
        update();
    }
    QWidget::mousePressEvent(event);
}

void RankEditor::mouseReleaseEvent(QMouseEvent *event){
    emit editingFinished();
    QWidget::mouseReleaseEvent(event);
}

int RankEditor::circleAtPosition(int x) const{
    int rank = (x / (100/5)) + 1;
    if (rank < 0 || rank > 5)
        return -1;
    return rank;
}
