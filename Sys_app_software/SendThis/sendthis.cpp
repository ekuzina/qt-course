#include "sendthis.h"
#include "ui_sendthis.h"
#include <QTextTable>
#include <QScrollBar>
#include <QTime>
SendThis::SendThis(QWidget *parent, QString Name, quint16 Port)
    : QMainWindow(parent)
    , ui(new Ui::SendThis), m_name(QString(Name)), m_port(Port)
{
    ui->setupUi(this);
    ui->lineEdit->setFocusPolicy(Qt::StrongFocus);
    ui->textEdit->setFocusPolicy(Qt::NoFocus);
    ui->textEdit->setReadOnly(true);
    ui->listWidget->setFocusPolicy(Qt::NoFocus);
     ui->listWidget->setSortingEnabled(true);
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(m_port, QAbstractSocket::ShareAddress|QAbstractSocket::ReuseAddressHint);
    connect(udpSocket, SIGNAL(readyRead()),
            this, SLOT(processPendingDatagrams()));
    connect(ui->lineEdit, SIGNAL(returnPressed()),
            this, SLOT(returnPressed()));
    connect(this, SIGNAL(createDeleteParticipant(char)),
            this, SLOT(inOutParticipant(char)));
    connect(this, SIGNAL(newPartincipantReplay(QString)),
            this, SLOT(newPartincipantReplayRecieved(QString)));
    setWindowTitle(tr("[%1@%2] - SendThis") .arg(name()) .arg(port()));
    ui->listWidget->addItem(name());
    emit createDeleteParticipant(1);
}

SendThis::~SendThis(){
    delete ui;
}

void SendThis::broadcastDatagram(QByteArray datagram){
    udpSocket->writeDatagram(datagram, QHostAddress::Broadcast, port());
}
void SendThis::processPendingDatagrams()
{
    QByteArray datagram;
    while (udpSocket->hasPendingDatagrams()) {
        datagram.resize(int(udpSocket->pendingDatagramSize()));
        udpSocket->readDatagram(datagram.data(), datagram.size());
        QDataStream dataStream (datagram);
        QPair<QString, QString> message;
        dataStream >> message.first >> message.second;
        if(message.second.length()==1){
            char sysMessage = (message.second.toStdString()[0]);
            if(sysMessage==1 && (message.first!=name()) ){
                ui->listWidget->addItem(message.first);
                emit newPartincipantReplay(message.first);
            }
            if(sysMessage==2 && (message.first!=name())){
                QList<QListWidgetItem *> items = ui->listWidget->findItems(message.first, Qt::MatchExactly);
                if (items.isEmpty())
                    return;
                delete items.at(0);
            }
        }
        else if(message.second.contains("newPartincipantReplayRecieved")){
            if(message.second.contains(name())) ui->listWidget->addItem(message.first);
        }
        else{
            appendMessage(message);
        }
    }
}
void SendThis::appendMessage(QPair<QString, QString> message){
    if (message.first.isEmpty() || message.second.isEmpty())
        return;
    ui->textEdit->setTextColor(message.first == name() ? Qt::red : Qt::blue);
    ui->textEdit->append(tr("[%1] ") .arg(QTime::currentTime().toString("hh:mm:ss")) );
    ui->textEdit->setFontWeight(QFont::Bold);
    ui->textEdit->insertPlainText(tr("%1") .arg(message.first));
    ui->textEdit->setTextColor(Qt::black);
    ui->textEdit->setFontWeight(QFont::Normal);
    ui->textEdit->insertPlainText(tr(": %1") .arg(message.second));
}

void SendThis::returnPressed(){
    QString messageText = ui->lineEdit->text();
    if (messageText.isEmpty())
        return;
    QByteArray datagram;
    QDataStream datagramStream( &datagram, QIODevice::WriteOnly);
    datagramStream << name() << messageText;
    broadcastDatagram(datagram);
    ui->lineEdit->clear();
}

void SendThis::inOutParticipant(char sysCode){
    QByteArray systemDatagram;
    QDataStream datagramStream( &systemDatagram, QIODevice::WriteOnly);
    datagramStream << name() << QString(sysCode);
    broadcastDatagram(systemDatagram);
}

void SendThis::newPartincipantReplayRecieved(QString nameOfNew){
    QByteArray systemDatagram;
    QDataStream datagramStream( &systemDatagram, QIODevice::WriteOnly);
    datagramStream << name() << QString("newPartincipantReplayRecievedFrom"+nameOfNew);
    broadcastDatagram(systemDatagram);
}

void SendThis::closeEvent(QCloseEvent *e){
    emit createDeleteParticipant(2);
}
