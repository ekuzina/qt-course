#ifndef SENDTHIS_H
#define SENDTHIS_H

#include <QMainWindow>
//#include <QtNetwork>
#include <QUdpSocket>
#include <QTextTableFormat>
QT_BEGIN_NAMESPACE
namespace Ui { class SendThis; }
QT_END_NAMESPACE

class SendThis : public QMainWindow
{
    Q_OBJECT

public:
    SendThis(QWidget *parent=nullptr, QString name="new sender", quint16 port=45454);
    quint16 port(){return m_port;}
    void setPort(quint16 port){m_port=port;}
    QString name(){return m_name;}
    void setName(QString name){m_name=name;}
    void closeEvent(QCloseEvent *e);
    ~SendThis();

private:
    Ui::SendThis *ui;
signals:
    void createDeleteParticipant(char);
    void newPartincipantReplay(QString);
public slots:
    void appendMessage(QPair<QString, QString> message);
private slots:
    void returnPressed();
    void broadcastDatagram(QByteArray);
    void processPendingDatagrams();
    void inOutParticipant(char);
    void newPartincipantReplayRecieved(QString);

private:
    QUdpSocket *udpSocket = nullptr;
    QString m_name;
    quint16 m_port;
};
#endif // SENDTHIS_H
