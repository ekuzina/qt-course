#include <QObject>
#include "sendthis.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QMessageBox>
#include <QSettings>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCommandLineParser parser;
    QCommandLineOption nameOpt("n", "sender name.", "new sender", "new sender");
    parser.addOption(nameOpt);
    QCommandLineOption portOpt("p", "port to connect.", "45454", "45454");
    parser.addOption(portOpt);
    parser.process(a);
    QString port = parser.value(portOpt);
    QString name = parser.value(nameOpt);
    if(QStringList(name)==nameOpt.defaultValues() && QStringList(port)==portOpt.defaultValues()){
           QString s("Open SendThis(c) with default parameters:\nname is %1, port is %2.\nUse command-line options to set yours:\n-n name -p port (integer)");
           QMessageBox msgBox;
           msgBox.setIcon(QMessageBox::Information);
           msgBox.setText(s.arg(name).arg(port));
           msgBox.exec();
    }
    SendThis participant(nullptr, name, static_cast<quint16>(port.toInt()));
    participant.show();
    return a.exec();
}
