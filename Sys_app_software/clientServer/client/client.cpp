#include "client.h"
#include "ui_client.h"
#include <QMessageBox>
#include <QTimer>
//#include <QtNetwork/QAbstractSocket>
//#include <QtNetwork/QHostAddress>
#include <QNetworkInterface>
#include <QTime>
Client::Client(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Client)
    , tcpSocket(new QTcpSocket(this))
    , udpSocket(new QUdpSocket(this))
{
    ui->setupUi(this);
    in.setDevice(tcpSocket);
    connect(ui->pushButton, &QAbstractButton::clicked,
            this, &Client::requestCurrentText);
    connect(ui->pushButton_2, &QAbstractButton::clicked,
            this, &Client::sendEdition);
    connect(tcpSocket, &QIODevice::readyRead, this, &Client::readServerAnswer);
    connect(tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
            this, &Client::displayError);
    connect(ui->lineEdit, SIGNAL(returnPressed()),
                this, SLOT(returnPressed()));
    connect(udpSocket, SIGNAL(readyRead()),
            this, SLOT(processPendingDatagrams()));
    sessionOpened();
}

Client::~Client()
{
    delete ui;
}

void Client::requestCurrentText(){
    if(ui->textEdit_2->toPlainText()!=QString("")){
        int n=QMessageBox::warning(this,"Client","Your edition will be lost. Continue?","Yes", "No", nullptr, 1, 0);
        if(n!=0)
            return;
    }
    ui->textEdit_2->clear();
    m_currentText=QString("");
    ui->pushButton->setEnabled(false);
    tcpSocket->abort();
    tcpSocket->connectToHost(m_address, m_port);
    request=true;
}

void Client::readServerAnswer(){
    in.startTransaction();

    QString nextText;
    in >> nextText;
    if (!in.commitTransaction())
        return;
    if(nextText == QString("server_getConnection")){
        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        if (request)
            out << QString("textRequest");
        else
            out << m_currentText;
       qint64 bytes = tcpSocket->write(block);
       ui->pushButton_2->setEnabled(true);
    }
    else{
        if (nextText == m_currentText) {
            QTimer::singleShot(0, this, &Client::requestCurrentText);
            return;
        }
        m_currentText = nextText;
        ui->textEdit_2->setText(m_currentText);
        ui->pushButton->setEnabled(true);
    }
}
void Client::sendEdition(){
    ui->pushButton_2->setEnabled(false);
    tcpSocket->abort();
    tcpSocket->connectToHost(m_address, m_port);
    m_currentText=ui->textEdit_2->toPlainText();
    ui->textEdit_2->clear();
    request=false;
}
void Client::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The host was not found. Please check the "
                                    "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The connection was refused by the peer. "
                                    "Make sure the server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Client"),
                                 tr("Error occurred: %1.")
                                 .arg(tcpSocket->errorString()));
    }

    ui->pushButton->setEnabled(true);
}
void Client::sessionOpened()
{

    QMessageBox::information(this, tr("Client"), tr("This app requires that you run the "
                            "Server as well."));
    ui->pushButton->setEnabled(true);
    if(!udpSocket->bind(m_port, QAbstractSocket::ShareAddress|QAbstractSocket::ReuseAddressHint)){
        QMessageBox::information(this, tr("Client"),
                                 tr("UDP binding failed. Change port and press <ENTER>"));
    }
}
void Client::processPendingDatagrams()
{
    if(m_time!=QTime::currentTime()){
    	setChangesStyle(Qt::blue,QFont::Bold);
    	ui->textEdit->append(tr("[%1]") .arg(QTime::currentTime().toString("hh:mm:ss")) );
    	m_time=QTime::currentTime();
    }
    QByteArray datagram;
    while (udpSocket->hasPendingDatagrams()) {
        datagram.resize(int(udpSocket->pendingDatagramSize()));
        udpSocket->readDatagram(datagram.data(), datagram.size());
        if (datagram.isEmpty())
            return;
        QDataStream dataStream (datagram);
        QPair<QString, QString> message;
        dataStream >> message.first >> message.second;
        setChangesStyle(Qt::black,QFont::Normal);
        if(message.second.isEmpty())
            ui->textEdit->append(message.first);
        else
            highlighting(&message);
    }
    if(ui->textEdit_2->toPlainText()!=QString("")){
        int n=QMessageBox::information(this,"Client","Text was changed. Load new?","Yes", "No", nullptr, 1, 0);
        if(n==0)
            requestCurrentText();
    }

}
void Client::returnPressed(){
    bool ok;
    int new_port = ui->lineEdit->text().toInt(&ok);
    if(ok && new_port!=m_port){
        if(!udpSocket->bind(static_cast<quint16>(new_port), QAbstractSocket::ShareAddress|QAbstractSocket::ReuseAddressHint)){
            QMessageBox::warning(this, tr("Client"),
                                     tr("UDP binding failed. Try again."));
            ui->textEdit->setText(QString("Can't get changes because of UDP binding failed."));
            //return;
        }
        else
            m_port=static_cast<quint16>(new_port);
    }
}

void Client::highlighting(QPair<QString,QString>* pair){
    pair->first.remove(0,1);
    int closeBra = pair->first.indexOf(']');
    int lineN = (pair->first.left(closeBra)).toInt();
    pair->first.remove(0,closeBra+1);
    const QChar sep = ' ';
    QStringList lineOld = pair->first.split(sep);
    QStringList lineNew = pair->second.split(sep);
    int sizeMin= (lineNew.size()<lineOld.size()) ? lineNew.size():lineOld.size();
    int sizeMax= (lineNew.size()>lineOld.size()) ? lineNew.size():lineOld.size();
    QString message("");
    message+=tr("{line %1}: ").arg(lineN);
    for(int i=0;i<sizeMin;i++){
            //same words
            if(lineNew.at(i)==lineOld.at(i)){
                message+=QString("<font color=\"black\">%1</font>").arg(lineNew.at(i))+QString(sep);
            }
            //most probably punctuation mark added or deleted
            else if(pair->first.contains(lineOld.at(i))|| pair->second.contains(lineNew.at(i))){
                message+=QString("<font color=\"magenta\" >%1</font>").arg(lineNew.at(i))+QString(sep);
            }
            //different words
            else{
                message+=QString("<font color=\"red\">%1</font>").arg(lineNew.at(i))+QString(sep);
            }

    }
    //less words are in new version
    if(lineNew.size()<sizeMax){
        message+=QString("<font color=\"red\">...deleted %1</font>").arg(sizeMax-lineNew.size());
        message+=QString("<font color=\"red\"> word(s)</font>");
    }
    //more words are in new version
    for(int i=sizeMin;i<lineNew.size();i++)
         message+=QString("<b>%1</b>").arg(lineNew.at(i))+QString(sep);
    ui->textEdit->append(message);

}
void Client::setChangesStyle(unsigned int color, int weight){
    ui->textEdit->setTextColor(color);
    ui->textEdit->setFontWeight(weight);
}
