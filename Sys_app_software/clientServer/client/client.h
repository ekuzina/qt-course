#ifndef CLIENT_H
#define CLIENT_H

#include <QDialog>
#include <QDataStream>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QUdpSocket>
#include <QTime>

QT_BEGIN_NAMESPACE
namespace Ui { class Client; }
QT_END_NAMESPACE

class Client : public QDialog
{
    Q_OBJECT

public:
    Client(QWidget *parent = nullptr);
    ~Client();
private:
    Ui::Client *ui;
private slots:
    void returnPressed();
    void requestCurrentText();
    void readServerAnswer();
    void sendEdition();
    void displayError(QAbstractSocket::SocketError socketError);
    void sessionOpened();
    void processPendingDatagrams();

private:
    QTcpSocket* tcpSocket = nullptr;
    QUdpSocket* udpSocket = nullptr;
    QDataStream in;
    QString m_currentText;
    quint16 m_port = 54545;
    QTime m_time;
    QHostAddress m_address = QHostAddress::LocalHost;
    bool request=true;
    void highlighting(QPair<QString,QString>* pair);
    void setChangesStyle(unsigned int color, int weight);

};
#endif // CLIENT_H
