#include "server.h"
#include <QCoreApplication>
#include <iostream>
#include <QtNetwork/QTcpSocket>
#include <QDataStream>
Server::Server(){

    sessionOpened();
    connect(tcpServer, &QTcpServer::newConnection, this, &Server::getConnection);
    connect(this, SIGNAL(textChanged(QString*)), this,SLOT(selectChanges(QString*)));

}
Server::~Server(){
    tcpServer->close();
    delete currentText;
};
void Server::sessionOpened(){
    tcpServer = new QTcpServer(this);
    if (!tcpServer->listen(QHostAddress::Any,m_port)){
        if(!tcpServer->listen()){
            std::cerr<<"Server\n"
                       "Unable to start the server:"<<
                        tcpServer->errorString().toStdString()<<std::endl;
            return;
        }
    }
    QString ipAddress;
    ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    std::cout<<"The server is running on\n\nIP:"<<ipAddress.toStdString()<<"\nport: "
              <<tcpServer->serverPort()<<"\n\n"
              <<"Run the Client now."<<std::endl;
    if(tcpServer->serverPort()!=m_port){
        std::cout<<"[WARNING]:Select right port in client application."<<std::endl;
        m_port=tcpServer->serverPort();
    }
    udpSocket = new QUdpSocket(this);
    std::cout<<"Server history..."<<std::endl;
}

void Server::getConnection(){
    clientConnection = tcpServer->nextPendingConnection();
    std::cout<<"\n"<<++historyCount<<". New connection detected from"
            <<" adress"<<clientConnection->peerAddress().toString().toStdString()
            <<". Preparing answer..."<<std::endl;
    connect(clientConnection, &QIODevice::readyRead, this, &Server::readMessage);
    connect(clientConnection, &QAbstractSocket::disconnected, clientConnection, &QObject::deleteLater);
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out << QString("server_getConnection");
    in.setDevice(clientConnection);
    qint64 bytes = clientConnection->write(block);
    std::cout<<QString(QString(historyCount).length()+1, ' ').toStdString()<<" Write answer "<<bytes<<std::endl;
}
void Server::readMessage(){
    in.startTransaction();
    QString* message = new QString();
    in >> *message;
    if (!in.commitTransaction() || message->isEmpty()){
        clientConnection->disconnectFromHost();
        disconnectClient();
        return;
    }
    if(*message==QString("textRequest")){
       sendCurrentText();
       return;
    }
   changeText(message);
}
void Server::sendCurrentText(){
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out << *currentText;
    clientConnection->write(block);
    std::cout<<QString(QString(historyCount).length()+1, ' ').toStdString()
             <<" Current text was sent."<<std::endl;
    clientConnection->disconnectFromHost();
    disconnectClient();
}
void Server::changeText(QString* newText){
    emit(textChanged(newText));
    std::cout<<QString(QString(historyCount).length()+1, ' ').toStdString()
             <<" The text is changed."<<std::endl;
    delete currentText;
    currentText=newText;
    clientConnection->disconnectFromHost();
    disconnectClient();
}
void Server::disconnectClient(){
    std::cout<<QString(QString(historyCount).length()+1, ' ').toStdString()
                                      <<" Disconnect."<<std::endl;
}
void Server::broadcastDatagram(QByteArray* datagram){
    std::cout<<QString(QString(historyCount).length()+1, ' ').toStdString()<<" Broadcasting text changes..."<<std::endl;
    udpSocket->writeDatagram(*datagram, QHostAddress::Broadcast, m_port);
}
void Server::selectChanges(QString* newText){
    QStringList linesNew = newText->split('\n');
    QStringList linesOld = currentText->split('\n');
    //different number of lines
    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::WriteOnly);
    if(linesNew.size()!=linesOld.size()){
        out << QString("Number of lines is changed: %1 is now, %2 was/were before.")
               .arg(linesNew.size())
               .arg(linesOld.size())
             << QString("");
        broadcastDatagram(&datagram);
    }
    //compare line to line
    int sizeMin= (linesNew.size()<linesOld.size()) ? linesNew.size():linesOld.size();
    for(int i=0;i<sizeMin;i++){
        if(linesOld.at(i)==linesNew.at(i)) continue;
        QByteArray datagram;
        QDataStream out(&datagram, QIODevice::WriteOnly);
        out << QString("[%1]").arg(i+1)+linesOld.at(i)<<linesNew.at(i);
        broadcastDatagram(&datagram);
    }
}
