#ifndef SERVER_H
#define SERVER_H
#include <QObject>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QUdpSocket>
#include <QtNetwork/QTcpSocket>
#include <QDataStream>

class Server: public QObject{
    Q_OBJECT

public:
    explicit Server();
    ~Server();
signals:
    void textChanged(QString*);
private slots:
    void sessionOpened();
    void selectChanges(QString*);
    void getConnection();
    void readMessage();

private:
    QTcpServer *tcpServer = nullptr;
    QUdpSocket *udpSocket = nullptr;
    QDataStream in;
    quint16 m_port = 54545;
    unsigned int historyCount=0;
    QTcpSocket* clientConnection=nullptr;
    QString* currentText = new QString("original text");
    void sendCurrentText();
    void changeText(QString*);
    void disconnectClient();
    void broadcastDatagram(QByteArray*);
};

#endif // SERVER_H
